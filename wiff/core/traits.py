import json
import logging
from collections import ChainMap
from contextlib import contextmanager
from pathlib import Path
from pprint import pformat

from . import utils

base_logger = logging.getLogger('wiff')
base_logger.setLevel(logging.DEBUG)
# base_logger.addHandler(logging.NullHandler())

root_logger = logging.getLogger('wiff')
root_logger.setLevel(logging.NOTSET)

class Identification:
    """Trait providing identification

    Arguments
    ---------
    identifier : str
        human readable id
    """

    def __init__(self, identifier, *args, **kwargs):
        #root_logger.debug( utils.class_mark(__class__) )

        # self.id = identifier
        self.__id = identifier
        self.__repr = f'<{self.class_id}/{self.id}>'

        super().__init__(*args, **kwargs)

    def __str__(self) -> str:
        return self.id

    def __repr__(self) -> str:
        return self.__repr

    @property
    def id(self):
        """instance identifier"""
        return self.__id

    # @id.setter
    # def id(self, value):
    #     self.identifier = value

    @property
    def class_id(self):
        """class identifier"""
        return utils.class_id( type(self) )


class Logging:
    """Trait providing logging capabilities

    Arguments
    ---------
    _logger_name : str
    _logger_root : logger
    _logger_level
    _logger_group : bool
    """

    default_logger_root = root_logger
    default_logger_level = logging.NOTSET
    default_logger_grouping = False

    def __init__(self, *args, _logger_name=None, _logger_root=None,
                           _logger_level=None, _logger_grouping=None, **kwargs):
        #root_logger.debug( utils.class_mark(__class__) )

        class_name = type(self).__name__
        name = _logger_name or class_name

        root = _logger_root or self.default_logger_root
        grouping = self.default_logger_grouping if _logger_grouping is None \
                                                           else _logger_grouping
        if grouping:
            root = root.getChild(class_name)

        level = _logger_level or self.default_logger_level

        self.logger = root.getChild(name)
        self.logger.setLevel(level)

        # logger_name_ = _logger_name or self.default_logger_name()
        # self.logger = logging.getLogger(logger_name_)
        # self.logger.setLevel(_logger_level)

        super().__init__(*args, **kwargs)

    # @property
    # def logger(self):
    #     return self.__logger

    # @logger.setter
    # def logger(self, value):
    #     self.__logger = value

    @property
    def base_logger(self):
        return base_logger

    def default_logger_name(self):
        base_name = self.base_logger.name
        prefix = '' if base_name == 'root' else f'{base_name}.'
        return f'{prefix}{utils.instance_id(self)}'

    def set_parent_logger(self, parent):
        _, _, child_logger_name = self.logger.name.rpartition('.')
        self.logger = parent.getChild(child_logger_name)

    # def new_file_logger(self, path):
    def new_file_logger(self, path, level=logging.INFO, formatter=None,
                                                                  filters=None):
        """adds file logging"""

        file_handler = logging.FileHandler(path)
        # file_handler.setLevel(level)

        # if formatter:
        #     file_handler.setFormatter(formatter)

        # for filter_name in ( filters or tuple() ):
        #     file_handler.addFilter( logging.Filter(filter_name) )

        self.logger.addHandler(file_handler)
        return file_handler

    def close_file_logger(self, handler):
        """removes file logging"""

        self.logger.removeHandler(handler)
        handler.close()

    @contextmanager
    def logging_to_file(self, path):
        """file logging context manager"""

        file_logger = self.new_file_logger(path)
        yield file_logger

        self.close_file_logger(file_logger)

    # TODO: wrapper to log (and perhaps basic profile) function calling


import inspect
from collections import namedtuple


class Registry:
    """Trait managing Inputs and Outputs

    Arguments
    ---------
    _registry_strict : bool
    _registry_debug : bool

    Tip
    ---
    Inputs work as regular :meth:`__init__` arguments and may be provided on
    instance creation without special treatment.
    """

    __input_class = namedtuple('Input', 'default alias optional output')

    @classmethod
    def Input(cls, default=None, *, alias=None, optional=False, output=False):
        """declaration to feed a class instance attribute"""

        return cls.__input_class(default, alias, optional, output)

    __output_class = namedtuple('Output', 'default alias')

    @classmethod
    def Output(cls, default=None, *, alias=None):
        """declaration to export a class instance attribute"""

        return cls.__output_class(default, alias)

    @classmethod
    def init_from(cls, context, *args, **kwargs):
        #print(cls, context, args, kwargs)

        # def resolve_input(name, details):
        #     context_name = details.alias or name
        #     value = context.get(context_name) or details.default
        #     return name, value

        # context_kwargs = dict( resolve_input(name, details)
        #     for name, details in cls.__input_members()
        #                                      if details.alias or name in context
        # )

        context_kwargs = dict()

        if context:
            for name, details in cls.__input_members():
                input_name = details.alias or name

                try:
                    value = context[input_name]
                except KeyError:
                    continue

                context_kwargs[name] = value

        # kwargs takes precedence over context
        context_kwargs.update(kwargs)

        # print(args, kwargs, context_kwargs)
        return cls(*args, **context_kwargs)

    def __init__(self, *args, _registry_strict=True, _registry_debug=False,
                                                                      **kwargs):
        self.__debug = _registry_debug

        self.__init_registry(kwargs, _registry_strict)

        if kwargs:
            root_logger.warning("%s's REMAINING: %s", self,
                                                    ', '.join( kwargs.keys() ) )

        super().__init__(*args, **kwargs)

    def __init_registry(self, kwargs, strict):
        inited_ = lambda default: default() if callable(default) else default

        self.__defined = set()

        for name, details in self.__input_members():
            # print(name, details)
            # value = kwargs.pop(name, details.default) if details.optional \
            #                                                else kwargs.pop(name)

            try:
                value = kwargs.pop(name)
                self.__defined.add(name)

            except KeyError:
                if details.optional or details.default is not None:
                    value = inited_(details.default)

                elif strict:
                    raise KeyError(
                              f'{type(self).__name__}.{name} must be supplied!')

                else:
                    root_logger.warning("%s MIA!",
                                                f'{type(self).__name__}.{name}')
                    continue

            self.__set_value(name, value)

        for name, details in self.__output_members():
            self.__set_value(name, inited_(details.default) )

        # for defined in self.__defined:
        #     init_fn = getattr(self, f'_init_{defined}', None)
        #     if init_fn:
        #         init_fn()
        #         root_logger.debug('calling %s with %s', init_fn, value)

    def __set_value(self, name, value):
        setattr(self, name, value)
        if self.__debug:
            root_logger.debug('%s = %s', f'{self!r}.{name}', pformat(value) )

    @classmethod
    def __input_members(cls, only_required=False):
        return tuple( cls.__members_of_(cls.__input_class) )

    @classmethod
    def __output_members(cls, include_inputs=False):
        output_members = cls.__members_of_(cls.__output_class)

        if include_inputs:
            output_members.extend( (name, details)
                  for name, details in cls.__input_members() if details.output )

        return tuple(output_members)

    @classmethod
    def __members_of_(cls, classinfo):
        specific_class_members = lambda member: isinstance(member, classinfo)
        return inspect.getmembers(cls, specific_class_members)

    def _get_inputs(self):
        return dict(
            ( name, getattr(self, name, None) )
                                     for name, details in self.__input_members()
        )

    def _get_outputs(self):
        return dict(
            ( details.alias or name, getattr(self, name) )
                 for name, details in self.__output_members(include_inputs=True)
        )

    def _inject_from(self, context, strict=False):
        root_logger.debug("defined before: %s", self.__defined)

        for name, details in self.__input_members():
            input_name = details.alias or name

            if name in self.__defined:
                root_logger.warning("%s is already defined!",
                                                f'{type(self).__name__}.{name}')
                continue

            try:
                value = context[input_name]
            except KeyError:
                # if strict and (not details.optional or details.default is None):
                if strict:
                    raise KeyError(
                              f'{type(self).__name__}.{name} must be supplied!')

                root_logger.debug("%s not injected, nothing in context fits!",
                                                f'{type(self).__name__}.{name}')
                continue

            self.__set_value(name, value)
            self.__defined.add(name)

        root_logger.debug("defined after: %s", self.__defined)


class Configuration:

    # TODO: read configuration from environment
    #       handle updates on nested confs

    # __conf_base = dict()

    # @classmethod
    # def conf_base(cls):
    #     return cls.__conf_base

    # @classmethod
    # def conf_base_update(cls, **conf):
    #     cls.__conf_base = cls.conf_base().copy()
    #     cls.__conf_base.update(conf)

    # __conf_keys = tuple()

    # @classmethod
    # def conf_keys(cls):
    #     return cls.__conf_keys

    # @classmethod
    # def conf_keys_extend(cls, keys):
    #     cls.__conf_keys += tuple(keys)

    conf_base = dict()
    conf_keys = tuple()


    def __init__(self, *args, _conf_base=None, _conf_dir=None, _conf_file=None,
                                                     _conf_keys=None, **kwargs):
        #root_logger.debug( utils.class_mark(__class__) )

        conf_filepath = Path( _conf_file or self.default_conf_file() )

        conf_path = _conf_dir / conf_filepath if _conf_dir else conf_filepath
        conf_path_ = utils.unfold_path(conf_path)

        self.__set_conf(conf_path_, base_conf=_conf_base)
        self.__check_required_keys(_conf_keys)

        super().__init__(*args, **kwargs)

    @property
    def conf(self):
        return self.__conf

    # @staticmethod
    # def _conf_dict_combine(*dict_list):
    #     conf = dict()
    #     for dict_ in dict_list:
    #         if dict_:
    #             conf.update(dict_)

    #     return conf

    def default_conf_file(self):
        return f'{utils.instance_id(self)}-conf.json'.lower().replace('/', '-')

    def __set_conf(self, conf_path, base_conf=None):
        user_conf = dict()
        init_conf = dict(base_conf) if base_conf else dict()
        file_conf = self.__read_file(conf_path)
        self_conf = dict(self.conf_base)

        self.__conf = ChainMap(user_conf, init_conf, file_conf, self_conf)

    def __read_file(self, conf_path):

        conf_file_status_log_msg_ = lambda status: (
                               f"configuration file '%s' {status}!", conf_path )

        try:
            conf = self.__read_json_file(conf_path)
        except FileNotFoundError:
            conf = dict()

            root_logger.debug( *conf_file_status_log_msg_('NOT FOUND') )
        else:
            root_logger.info( *conf_file_status_log_msg_('READ') )

        return conf

    @staticmethod
    def __read_json_file(json_path):
        json_str = json_path.read_text()
        return json.loads(json_str)

    def __check_required_keys(self, keys):
        keys_ = keys or self.conf_keys
        missing_keys = self.__conf_missing_keys(keys_)

        if missing_keys:
            raise KeyError(', '.join(missing_keys) +
                                      ' required conf key(s) is(/are) missing!')

    def __conf_missing_keys(self, keys):
        return set(keys).difference( self.conf.keys() )
