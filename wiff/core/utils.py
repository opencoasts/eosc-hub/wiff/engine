from datetime import timedelta
from inspect import Attribute
from pathlib import Path
from pprint import PrettyPrinter


def class_id(class_obj):
    return class_obj.__name__

def instance_id(inst_obj):
    return getattr(inst_obj, 'id', None) or class_id( type(inst_obj) )


def class_mark(class_obj, mark='__init__'):
    return f'{class_id(class_obj)}.{mark}'


def listing(parts, separator=' '):
    return separator.join( map(str, parts) )

def join_lines(*lines):
    return listing(lines, '\n')

def format_lines(*lines, **fmt_dict):
    return join_lines(*lines).format_map(fmt_dict)


_pretty_printer = PrettyPrinter(indent=2, width=80)

def pp(content):
    _pretty_printer.pprint(content)

def pf(content):
    return _pretty_printer.pformat(content)


ONE_HOUR = timedelta(hours=1)
ONE_DAY = timedelta(days=1)

def _convert_timedelta(time_delta, divisor):
    return time_delta / divisor

def hours_in_(time_delta):
    return _convert_timedelta(time_delta, ONE_HOUR)

def days_in_(time_delta):
    return _convert_timedelta(time_delta, ONE_DAY)

def timedelta_range(end, *, begin=None, delta=ONE_DAY):
    stop = int( end.total_seconds() + 1 )
    start = int( (begin or delta).total_seconds() )
    step = int( delta.total_seconds() )
    return ( timedelta(seconds=delta_seconds)
                                 for delta_seconds in range(start, stop, step) )

# def datetime_range(begin, period, delta=ONE_DAY):
#     return ( begin + increment
#                          for increment in timedelta_range(period, delta=delta) )


def unfold_path(*pathsegments, strict=False):
    return Path(*pathsegments).expanduser().resolve(strict=strict)

# TODO: change to proper try catch
# def assert_variable_type(name, scope, var_type, var_kind='variable'):
#   variable = scope.get(name)
#     assert isinstance(variable, var_type), (
#                   f'{name} {var_kind} must be a {var_type.__name__} instance')

#     return variable


import logging
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from functools import wraps
from time import sleep
from threading import Thread, Lock, Event, BoundedSemaphore, current_thread
from queue import Queue

root_logger = logging.getLogger('wiff')

class QueuedExecution:
    """Imposes limits on the concurrent execution of functions/methods

    Arguments
    ---------
    name='queue' : str
        queue reference name
    concurrency=1 : int
        concurrency slot count
    """

    # to mimic some @property behavior, which decorates with another object
    # each decorated method would be a new Wrapper instance, implementing
    # __call__, while allowing it to be a handler/proxy for the QueuedExecution
    # instance which created it as well
    class Wrapper:
        pass

    logger = root_logger.getChild('_queued_execution')

    def __init__(self, name='queue', concurrency=1):
        self.name = name
        self.logger = self.logger.getChild(name)

        self._concurrency = concurrency

        self._allocated = False
        self._allocate_lock = Lock()

    enabled = False

    @classmethod
    def enable(cls):
        """enables the limited concurrent execution"""

        cls.enabled = True
        cls.logger.debug("QueuedExecution enabled!")

    def set_concurrency(self, value):
        """sets the concurrent execution slot count"""

        with self._allocate_lock:
            if self._allocated:
                self.logger.debug(f'{self.name} queue concurrency cannot be '
                                     'changed because it is already allocated!')
            else:
                self._concurrency = value
                self.logger.debug(f'{self.name} queue concurrency = {value}!')

    def __call__(self, func):
        return self.decorator(func)

    def _monitoring(self):
        self.logger.debug(f"{current_thread()} monitoring queue...")

        while True:
            # self.logger.debug('pre', self.name, 'initial =', self.semaphore._initial_value,
            #                                  'current =', self.semaphore._value)
            # if self.semaphore._value:
            #     self.logger.debug(self.name, 'queue is free')
            # else:
            #     self.logger.debug(self.name, 'queue is busy')

            self.logger.debug(f"acquire {self.name}")
            self.semaphore.acquire()
            # self.logger.debug('post', self.name, 'initial =', self.semaphore._initial_value,
            #                                  'current =', self.semaphore._value)

            self.logger.debug(f"getting {self.name}")
            event = self.queue.get()
            self.logger.debug(f"setting {self.name}")
            event.set()
            self.queue.task_done()

            #self.logger.debug('ready!')

    # def bind(self, func):
    #     # self.logger.debug('bind')
    #     return self.decorator(func)

    def decorator(self, func):
        """decorator"""

        # self.logger.debug('decorator')
        logger = self.logger

        @wraps(func)
        def wrapper(*args, release_queue=True, **kwargs):
            # self.logger.debug('wrapper')

            try:
                logger = args[0].logger
            except (IndexError, AttributeError):
                pass

            if not self.enabled:
                logger.debug(f"{func.__qualname__} execution NOT queued!")
                return func(*args, **kwargs)

            self._allocate()

            ready_event = Event()
            self.queue.put(ready_event)

            thread = current_thread()
            logger.debug(f"queued {thread} @ {self.name}, waiting...")
            wait_begin = datetime.now()

            ready_event.wait()

            wait_delta = datetime.now() - wait_begin
            logger.debug(
                       f"queued {thread} @ {self.name} waited for {wait_delta}")

            try:
                result = func(*args, **kwargs)

            finally:
                # support for async calls, release becomes caller responsability
                if release_queue:
                    self._release()

            return result

        return wrapper

    def release(self):
        """releases the slot"""

        if self.enabled:
            self._release()

    def _release(self):
        self.semaphore.release()
        self.logger.debug(f"released {current_thread()} @ {self.name}")

    def _allocate(self):
        with self._allocate_lock:
            if not self._allocated:
                self.queue = Queue()
                self.semaphore = BoundedSemaphore(self._concurrency)
                thread = Thread(target=self._monitoring,
                                       name=f'{self.name}-monitor', daemon=True)
                thread.start()

                self.logger.debug(
                    f"{self.name} queue resources allocated, {thread} "
                        f"started (concurrency={self.semaphore._initial_value})"
                )
                self._allocated = True


if __name__ == '__main__':

    class A:
        queue = QueuedExecution(concurrency=1)

        @queue
        def test(payload):
            print('pre', payload)
            sleep(5)
            print('post', payload)

    class B(A):
        pass

    class C(A):
        pass

    with ThreadPoolExecutor(max_workers=10) as executor:
        #executor.map(test, range(10), )
        for i in range(10):
            executor.submit(C.test, i)
            sleep(1)
