import sys
import numpy as np
from datetime import datetime, timedelta
from netCDF4 import Dataset, num2date, date2num

from .schism import HorizontalGrid

def convert(elev_path, ugrid_path, hgrid_path=None, remove_sources=False):

    hvel_path = elev_path.replace('_elev.', '_hvel.')

    first_timestep_of_ = lambda time_var: \
              datetime.strptime(time_var.base_date.strip(), '%Y-%m-%d %H:%M:%S')

    #origin_time_var, origin_ele_var, origin_x_var, origin_y_var = None, None, None, None
    #origin_elev_var, origin_u_var, origin_v_var = None, None, None
    #origin_time_temp, origin_time_salt, origin_time_zcor, = None, None, None

    # Open origin netcdfs
    with Dataset(elev_path) as elev_ds, Dataset(hvel_path) as hvel_ds:
        # Get Time Variables
        origin_time_var_elev = elev_ds.variables['time']
        origin_time_var_hvel = hvel_ds.variables['time']

        # Get Time info
        base_date_elev = first_timestep_of_(origin_time_var_elev)
        base_date_hvel = first_timestep_of_(origin_time_var_hvel)

        # If both files have the same date reference data
        if not base_date_elev == base_date_hvel:
            sys.exit('>> Aborting data conversion!\n'
                     '>> 1_elev, 1_hvel files have different date references.')

        base_date = base_date_elev

        # Get origin Dimension sizes from elevation
        node_size = elev_ds.dimensions['node'].size
        nele_size = elev_ds.dimensions['nele'].size
        nface_size = elev_ds.dimensions['nface'].size
        time_size = elev_ds.dimensions['time'].size

        # Get elevation dimension from hvel
        nv_size = hvel_ds.dimensions['nv'].size

        # Get Variables
        origin_time_var = elev_ds.variables['time'][:]
        origin_ele_var = elev_ds.variables['ele'][:]
        origin_x_var = elev_ds.variables['x'][:]
        origin_y_var = elev_ds.variables['y'][:]
        origin_depth_var = elev_ds.variables['depth'][:]
        origin_elev_var = elev_ds.variables['elev'][:]
        origin_u_var = hvel_ds.variables['u'][:]
        origin_v_var = hvel_ds.variables['v'][:]

    if nv_size > 2:
        temp_path = elev_path.replace('_elev.', '_temp.')
        salt_path = elev_path.replace('_elev.', '_salt.')
        zcor_path = elev_path.replace('_elev.', '_zcor.')

        # Open origin netcdfs
        with Dataset(temp_path) as temp_ds, Dataset(salt_path) as salt_ds, \
                                                  Dataset(zcor_path) as zcor_ds:
            # Get Time Variables
            # origin_time_temp = temp_ds.variables['time']
            # origin_time_salt = salt_ds.variables['time']
            # origin_time_zcor = zcor_ds.variables['time']

            # # Get Time info
            # base_date_temp = first_timestep_of_(origin_time_temp)
            # base_date_salt = first_timestep_of_(origin_time_salt)
            # base_date_zcor = first_timestep_of_(origin_time_zcor)

            # # If both files have the same date reference data
            # if not base_date_elev == base_date_hvel == base_date_temp == base_date_salt == base_date_zcor:
            #     sys.exit('>> Aborting data conversion!\n'
            #                 '>> 1_temp, 1_salt, 1_zcor files have different date references.')

            # Get Variables
            origin_temp_var = temp_ds.variables['temp'][:]
            origin_salt_var = salt_ds.variables['salt'][:]
            origin_zcor_var = zcor_ds.variables['zcor'][:]

    wwm_outputs = False
    wwm_wsh_path = elev_path.replace('_elev.', '_wwm_1.')
    wwm_wsh_dir_path = elev_path.replace('_elev.', '_wwm_7.')
    wwm_tp_path = elev_path.replace('_elev.', '_wwm_9.')

    try:
        with Dataset(wwm_wsh_path) as wwm_wsh_ds, \
                                    Dataset(wwm_wsh_dir_path) as wwm_wsh_dir_ds:
            origin_wwm_wsh_var = wwm_wsh_ds.variables['wm_1'][:]
            origin_wwm_wsh_dir_var = np.deg2rad(
                                     wwm_wsh_dir_ds.variables['wm_7'][:] + 180.)

        wwm_wsh_u_var = origin_wwm_wsh_var * np.sin(origin_wwm_wsh_dir_var)
        wwm_wsh_v_var = origin_wwm_wsh_var * np.cos(origin_wwm_wsh_dir_var)

        with Dataset(wwm_tp_path) as wwm_tp_ds:
            origin_wwm_tp_var = wwm_tp_ds.variables['wm_9'][:]

        wwm_outputs = True

    except FileNotFoundError:
        print("No wave outputs!")

    # Create new out netcdf
    with Dataset(ugrid_path, 'w', format='NETCDF4_CLASSIC') as out_dataset:
        # Global Attributes
        out_dataset.Conventions = 'CF-1.4, UGRID-0.9'

        if nv_size > 2:
            out_dataset.title = '24H Elevation, Flow velocities, Temperature and Salinity Forecast Data'
        else:
            out_dataset.title = '24H Elevation and Flow velocities Forecast Data'

        out_dataset.institution = 'LNEC - www.lnec.pt'
        out_dataset.source = 'LNEC/DHA - www.lnec.pt/hidraulica-ambiente/en/'
        end_date = base_date + timedelta(hours=24)
        time_values = ','.join(str(int(round(x))) for x in np.array(origin_time_var).tolist())

        format_datetime_from_ = lambda dt: datetime.strftime(dt, '%Y-%m-%d %H:%M:%S')
        formatted_base_date = format_datetime_from_(base_date)

        # Time Global Attributes
        out_dataset.date_created = format_datetime_from_(datetime.now())
        out_dataset.time_coverage_start = formatted_base_date
        out_dataset.time_coverage_end = format_datetime_from_(end_date)
        out_dataset.time_coverage_duration = 'P24H'
        out_dataset.time_coverage_resolution = 'P1H'
        out_dataset.NETCDF_DIM_time_VALUES = time_values

        # Create Dimensions
        out_dataset.createDimension('node', node_size)
        out_dataset.createDimension('face', nele_size)
        out_dataset.createDimension('max_face_nodes', nface_size)
        out_dataset.createDimension('time', time_size)
        if nv_size > 2:
            out_dataset.createDimension('elevation', nv_size)

        def new_variable(name, values, dims, attrs, np_type=np.float32,
                                 fill_value=-999, least_sig_digit=3, zlib=True):

            var = out_dataset.createVariable(name, np_type, dims, zlib=True,
                      fill_value=-999., least_significant_digit=least_sig_digit)
            var.setncatts(attrs)
            var[:] = values

        # Create Variables
        time_var = out_dataset.createVariable('time', np.float32, ('time',))
        mesh_var = out_dataset.createVariable('Mesh', np.int32)
        face_nodes_var = out_dataset.createVariable('face_nodes', np.int32, ('max_face_nodes', 'face',), fill_value=-999)
        node_lon_var = out_dataset.createVariable('node_lon', np.float32, ('node',))
        node_lat_var = out_dataset.createVariable('node_lat', np.float32, ('node',))
        depth_var = out_dataset.createVariable('depth', np.float32, ('node',))
        elev_var = out_dataset.createVariable('elev', np.float32, ('time', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
        if nv_size > 2:
            wind_u_var = out_dataset.createVariable('u', np.float32, ('time', 'elevation', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            wind_v_var = out_dataset.createVariable('v', np.float32, ('time', 'elevation', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            temp_var = out_dataset.createVariable('temp', np.float32, ('time', 'elevation', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            salt_var = out_dataset.createVariable('salt', np.float32, ('time', 'elevation', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            zcor_var = out_dataset.createVariable('zcor', np.float32, ('time', 'elevation', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            elevation_var = out_dataset.createVariable('elevation', np.int8, ('elevation',))
        else:
            wind_u_var = out_dataset.createVariable('u', np.float32, ('time', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)
            wind_v_var = out_dataset.createVariable('v', np.float32, ('time', 'node',), least_significant_digit=3, fill_value=-999., zlib=True)

        if wwm_outputs:
            new_wwm_variable = lambda attrs, **kwargs: new_variable(
                dims = ('time', 'node'),
                attrs = dict(
                    mesh = 'Mesh',
                    location = 'node',
                    coordinates = 'node_lon node_lat',
                    **attrs,
                ),
                **kwargs,
            )

            new_wwm_variable(
                name = 'wsh_u',
                values = wwm_wsh_u_var,
                attrs = dict(
                    standard_name =
                              'u-sea_surface_wave_significant_height_component',
                    long_name =
                            "U-component of Sea Surface Wave Significat Height",
                    units = 'm',
                ),
            )
            new_wwm_variable(
                name = 'wsh_v',
                values = wwm_wsh_v_var,
                attrs = dict(
                    standard_name =
                              'v-sea_surface_wave_significant_height_component',
                    long_name =
                            "V-component of Sea Surface Wave Significat Height",
                    units = 'm',
                ),
            )
            new_wwm_variable(
                name = 'tp',
                values = origin_wwm_tp_var,
                attrs = dict(
                    standard_name = 'sea_surface_wind_wave_period',
                    long_name = "Sea Surface Wind Wave Period",
                    units = 's',
                ),
            )

        # Set Attributes
        time_var.units = 'hours since 1970-01-01 00:00:00 UTC'
        time_var.setncatts({
            'standard_name': "time",
            'long_name': "Time",
        })

        mesh_var.setncatts({
            'node_coordinates': "node_lon node_lat",
            'topology_dimension': 2,
            'cf_role': "mesh_topology",
            'long_name': "Topology data of 2D unstructured mesh",
            'face_node_connectivity': "face_nodes"
        })

        face_nodes_var.setncatts({
            'start_index': 1,
            'units': "non-dimensional",
            'cf_role': 'face_node_connectivity',
            'long_name': 'Maps every face to its corner nodes'
        })

        node_lon_var.setncatts({
            'standard_name': "longitude",
            'mesh': "Mesh",
            'units': "degrees_east",
            'long_name': "Longitude of 2D mesh nodes"
        })

        node_lat_var.setncatts({
            'standard_name': "latitude",
            'mesh': "Mesh",
            'units': "degrees_north",
            'long_name': "Latitude of 2D mesh nodes"
        })

        depth_var.setncatts({
            'standard_name': "sea_floor_depth_below_sea_level",
            'mesh': "Mesh",
            'location': "node",
            'units': "m",
            'positive': "down",
            'coordinates': "node_lon node_lat",
            'long_name': "Bathymetry"
        })

        elev_var.setncatts({
            'standard_name': "sea_surface_height_above_sea_level",
            'mesh': "Mesh",
            'location': "node",
            'units': "m",
            'coordinates': "node_lon node_lat",
            'long_name': "Sea surface elevation",
            'positive': "up",
        })

        wind_u_var.setncatts({
            'standard_name': "eastward_sea_water_velocity",
            'mesh': "Mesh",
            'location': "node",
            'coordinates': "node_lon node_lat",
            'long_name': "Eastward water velocity",
            'missing_value': -9999.
        })

        wind_v_var.setncatts({
            'standard_name': "northward_sea_water_velocity",
            'mesh': "Mesh",
            'location': "node",
            'coordinates': "node_lon node_lat",
            'long_name': "Northward water velocity",
        })

        if nv_size > 2:
            temp_var.setncatts({
                'standard_name': "sea_water_temperature",
                'mesh': "Mesh",
                'location': "node",
                'coordinates': "node_lon node_lat",
                'long_name': "Water temperature",
            })

            salt_var.setncatts({
                'standard_name': "sea_water_practical_salinity",
                'mesh': "Mesh",
                'location': "node",
                'coordinates': "node_lon node_lat",
                'long_name': "Water practical salinity",
            })

            zcor_var.setncatts({
                'standard_name': "z_coordinates",
                'mesh': "Mesh",
                'location': "node",
                'coordinates': "node_lon node_lat",
                'long_name': "Z coordinates at whole levels",
                'units': "meters",
                'positive': "up"
            })

            elevation_var.setncatts({
                'long_name': "Whole levels",
                'standard_name': "sigma",
                'units': "non-dimensional",
                'positive': "up"
            })

        # Convert origin times to different time reference
        origin_time_dates = num2date(origin_time_var,
                                "seconds since {}".format(formatted_base_date) )
        time_data = date2num(origin_time_dates, time_var.units,
                                                            calendar='standard')

        # Fill variables
        time_var[:] = time_data

        if hgrid_path:
            hgrid = HorizontalGrid.open(hgrid_path)
            origin_x_var, origin_y_var = tuple( zip(*hgrid.nodes) )[:2]

        node_lon_var[:] = origin_x_var
        node_lat_var[:] = origin_y_var

        face_nodes_var[:] = origin_ele_var
        depth_var[:] = origin_depth_var
        elev_var[:] = origin_elev_var

        if nv_size > 2:
            wind_u_var[:] = origin_u_var
            wind_v_var[:] = origin_v_var
            temp_var[:] = origin_temp_var
            salt_var[:] = origin_salt_var
            zcor_var[:] = origin_zcor_var
            # Build levels variable
            elevation_var[:] = list(range(1, nv_size + 1))
        else:
            wind_u_var[:] = origin_u_var[:, 0, :]
            wind_v_var[:] = origin_v_var[:, 0, :]

        print('>> Conversion success!!')

if __name__ == '__main__':
    from sys import argv
    convert(*argv[1:])
