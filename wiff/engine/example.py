from wiff.engine.parts import Stage, Layer


class Prepare(Layer):

    date_time = Layer.Input()

    def execute(self):
        self.logger.info('\n >>> Prepering simulation environment for %s <<<',
                                                                 self.date_time)

    def finalize(self):
        self.logger.info('\n >>> Finalizing simulation environment for %s <<<',
                                                                 self.date_time)


class Compute(Stage):

    execution_date_time = Stage.Output()

    def execute(self):
        self.logger.info('\n >>> Execution simulation model <<<')
        self.execution_date_time = datetime.now()


class Finalize(Stage):

    execution_date_time = Stage.Input()

    def execute(self):
        self.logger.info('\n >>> Processing model results of run %s <<<',
                                                       self.execution_date_time)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.INFO)

    from datetime import datetime, timedelta, date, time

    today = datetime.combine( date.today(), time(0) )
    one_day = timedelta(days=1)
    tomorrow = today + one_day

    from wiff.engine.parts import Series, Simulation

    Series('forecast',
        begin = today,
        interval = one_day,
        end = tomorrow,

        simulation_templates = (

            Simulation.template('simulation',
                stage_templates = (
                    Prepare.template('a'),
                    Compute.template('b'),
                    Finalize.template('c'),
                )
            ),

        )
    ).simulate()
