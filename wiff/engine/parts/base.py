from pprint import pformat

from wiff.core.traits import Identification, Logging, Registry

# TODO: bring implementations details from trait classes to here
# eg: default names, paths, etc... ?
class Part0(Identification, Logging, Registry):
    """Base class for :mod:`wiff.engine.parts` classes

    The purpose of this class is to provide a common set of features available
    on :mod:`engine.core.parts` classes.

    Arguments
    ---------
    name : str
        human readable id
    kwargs
        base classes :meth:`__init__`'s keywork arguments

    Notes
    -----
    These features are split into traits, namely :class:`Identification`,
    :class:`Logging` and :class:`Registry`.
    All traits are encapsulated into their own classes, then combined to form
    this base class.
    """

    def __init__(self, name, **kwargs):
        kwargs.setdefault('_logger_name', name)
        super().__init__(name, **kwargs)

        self.logger.info(" ###> INITED <###")
        kwargs_items = '\n'.join(
                          f' # {item}' for item in pformat(kwargs).split('\n') )
        self.logger.debug(f"init kwargs:\n{kwargs_items}")

    # def __repr__(self) -> str:
    #     lines = (
    #         f"<{self}>",
    #          " inputs:",
    #         f" {pformat( self._get_inputs() )}",
    #          " outputs:",
    #         f" {pformat( self._get_outputs() )}",
    #     )
    #     return '\n'.join(lines)


    class Template:
        """Container for defining pre-initialized :mod:`engine.core.parts`"""

        def __init__(self, kind, name, kwargs):
            self.kind = kind
            self.name = name
            self.kwargs = kwargs

        def __str__(self) -> str:
            return f'{self.kind}/{self.name}'

        def __repr__(self) -> str:
            return f'{self.__class__.__name__}({self}) {self.kwargs}'

        def render(self, context=None, **kwargs):
            """spawns a new :mod:`engine.core.parts` instance"""
            """spawns a new :mod:`engine.core.parts` instance"""
            return self.kind.init_from(context, self.name,
                                                 **dict(self.kwargs, **kwargs) )

    @classmethod
    # def template(cls, **kwargs):
    def template(cls, name, **kwargs):
        """convenience class method for returning :class:`Template` instances"""

        return cls.Template(cls, name, kwargs)
