from core import utils
from core.entities import BaseEntity


class Manager(BaseEntity):
    '''Simulations manager'''
    def __init__(self, identifier, *args, **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        super().__init__(identifier, *args, **kwargs)