import inspect
from datetime import datetime, date, time, timedelta
from pathlib import Path
from pprint import pformat

from wiff.core import utils
from .base import Part0
from .simulation import Simulation0


class Series0(Part0):
    '''Simulation series'''

    @property
    def __init__begin__(self):
        return datetime.combine( date.today(), time(0) )
    # __init__begin__ = lambda _: datetime.combine( date.today(), time(0) )

    @property
    def __init__path__(self):
        return Path.cwd()
    # __init__path__ = lambda _: Path.cwd()

    __init__ws_dirname__ = '%Y/%m/%d/%H/{simulation.id}'
    # __init__ws_dirname = {date_time:%Y/%m/%d/%H}/{simulation.id}'

    def __init__(self, identifier, simulation, interval, *args, begin=None,
                                  end=None, path=None, ws_dirname='', **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        super().__init__(identifier, *args, **kwargs)

        if not isinstance(simulation, Simulation0):
            raise TypeError('simulation argument must be a Simulation instance'
                                           f', not {type(simulation).__name__}')

        simulation.set_parent('series', self)
        self.__simulation = simulation

        if not isinstance(interval, timedelta):
            raise TypeError('interval argument must be a timedelta instance'
                                             f', not {type(interval).__name__}')
        self.__interval = interval

        if begin and not isinstance(begin, datetime):
            raise TypeError('begin argument must be a datetime instance or None'
                                                f', not {type(begin).__name__}')
        self.__begin = begin or self.__init__begin__

        if end and not isinstance(end, datetime):
            raise TypeError('end argument must be a datetime instance or None'
                                                  f', not {type(end).__name__}')
        self.__end = end

        # just Path instances?
        if path and not ( isinstance(path, Path) or isinstance(path, str) ):
            raise TypeError('path argument must be a Path instance, str or None'
                                                 f', not {type(path).__name__}')
        path_ = path or self.__init__path__
        self.__path = utils.unfold_path(path_, strict=True)

        if ws_dirname and not isinstance(ws_dirname, str):
            raise TypeError('ws_dirname argument must be a str or None'
                                           f', not {type(ws_dirname).__name__}')

        ws_dirname_ = ws_dirname or self.__init__ws_dirname__
        self._ws_dirname = self.format_workspace_dirname(dirname=ws_dirname_)

        self._check_instalation()

        self.logger.debug(self)

    def __str__(self):
        return '; '.join( (
            repr(self),
            f'simulation: {repr(self.simulation)}',
            f'interval: {self.interval}',
            f'begin: {self.begin}',
            f'end: {self.end}',
            f'path: {self.path}',
            f'ws_dirname: {self._ws_dirname}',
        ) )

    @property
    def simulation(self):
        return self.__simulation

    @property
    def interval(self):
        return self.__interval

    @property
    def begin(self):
        return self.__begin

    @property
    def end(self):
        return self.__path

    @property
    def path(self):
        return self.__path

    @property
    def _next_simulation_datetime(self):
        next_date = date.today()
        next_time = self.begin.time()
        return datetime.combine(next_date, next_time)

    run_simulation_date_time = _next_simulation_datetime

    def run_simulation(self, *, date_time=None, **base_context):
        self.logger.debug('date_time=%s, **base_context=%s', date_time,
                                                                   base_context)

        if not isinstance(date_time, datetime):
            raise TypeError('date_time argument must be a datetime instance'
                                           f', not {type(date_time).__name__}')

        date_time_ = date_time or self.run_simulation_date_time

        workspace_dirpath = Path( self.format_workspace_dirname(date_time_) )
        workspace_path = self.path / workspace_dirpath
        self._create_workspace(workspace_path)

        context = dict(
            date_time = date_time_,
            path = workspace_path,

            series_interval = self.interval,
            series_path = self.path,
            series_dir = workspace_dirpath,
            series_path_template = str( self.path / self._ws_dirname ),
        )

        conflicting_keys = set( context.keys() ).intersection(
                                                           base_context.keys() )
        if conflicting_keys:
            self.logger.warning("context key(s): %s will be overwritten!",
                                                               conflicting_keys)
        context.update(base_context)

        self.logger.info("%s at '%s' on %s (contex: %s) STARTED!",
                               repr(self), workspace_path, date_time_, context)
        try:
            context = self.simulation.run(**context)
        except:
            self.logger.exception( "%s FAILED!", repr(self) )
        else:
            self.logger.info( "%s FINISHED!", repr(self) )

        return context

    def run_simulation_range(self):
        pass

    # TODO: rework (as workspace mixin?)
    def format_workspace_dirname(self, date_time=None, *, dirname='',
                                              extra_scope_dict=None, safe=True):

        dirname_ = getattr(self, '_ws_dirname', None)

        if dirname:
            scope_ = dict( inspect.getmembers(self) )
            if extra_scope_dict:
                scope_.update(extra_scope_dict)

            dirname_ = dirname.format_map(scope_)

        if date_time:
            dirname_ = date_time.strftime(dirname_)

        if safe:
            dirname_ = ( dirname_.
                            replace(':', '').
                            replace(':', '').
                            replace(' ', '_') )

        return dirname_

    def _check_instalation(self, path=None):
        '''perform some checks on the'''
        self.logger.debug("_check_instalation(path='%s')", path)

    # _create_workspace_dir_mode = 0o775 # o=rwx,g=rwx,a=rx

    def _create_workspace(self, path, dir_mode=0o775):
        self.logger.info("creating workspace folder '%s' (mode=%o)", path,
                                                                       dir_mode)
        path.mkdir(mode=dir_mode, parents=True, exist_ok=True)


class Series1(Part0):
    """Handles the temporal aspect of the forecast simulations

    The purpose of this class is to spawn and run its simulations (from
    `step_templates` definitions) with a regular `interval`, within a time
    period (`begin` and `end`).
    Simulations are inserted using templates defined in `simulation_templates`.
    `simulation_context` may be used to define a context to be included when a
    simulation is spawned.

    The order of execution follows the `simulation_templates` definition.
    All simulations are spawned and executed unless an exception is caught.

    Example
    -------
    .. code::

        Series1('forecast',
            simulation_templates = (
                Simulation.template('a',
                    ...
                ),
                Simulation.template('b',
                    ...
                ),
            ),
            simulation_context = (
                ...
            )
            begin = datetime(2020, 1, 1, 0, 0),
            interval = timedelta(hours=24),
            end = datetime(2020, 1, 3, 0, 0),
        ).simulate()
    """

    simulation_templates = Part0.Input(tuple)
    """sequence of simulation templates"""

    simulation_context = Part0.Input(dict)
    """common base context for all simulations"""

    # interactive = Part0.Input(False)

    begin = Part0.Input(alias='series_begin', output=True)
    """begin instant of the simulation spawn period"""

    interval = Part0.Input(alias='series_interval', output=True)
    """interval between spawned simulations"""

    end = Part0.Input(optional=True, alias='series_end', output=True)
    """end instant of the simulation spawn period"""

    date_time = Part0.Output()

    def _execution_context(self, **kwargs):
        return {
            **self._get_outputs(),
            **self.simulation_context,
            **kwargs,
            '_series': self,
        }

    def simulate(self):
        """performs the whole simulation process"""

        self.date_time = self.begin
        self.end = self.end or self.begin

        simulations_series = dict()
        while self.date_time <= self.end:

            simulations_map = dict()
            for template in self.simulation_templates:

                context = self._execution_context()
                # print('series.execution_context:', context)

                simulation = template.render(context, _logger_root=self.logger)

                logger_args_ = lambda state: ('##> %s %s @ %s <##', state,
                                                     simulation, self.date_time)

                self.logger.debug( *logger_args_('STARTING') )

                try:
                    simulation.run()
                except:
                    self.logger.exception( *logger_args_('FAILED') )
                    break

                self.logger.info( *logger_args_('FINISHED') )

                simulations_map[template.name] = simulation

            simulations_series[self.date_time] = simulations_map
            self.date_time += self.interval

        self.logger.debug('simulation series:\n%s',
                                                   pformat(simulations_series) )

        return simulations_series
