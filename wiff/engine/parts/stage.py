from wiff.core import utils
from .base import Part0
from ..mixins_ import Filiation


class Step0(Part0, Filiation):
    '''Simulation task'''

    def __init__(self, identifier, *args, dependencies=None, **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        dependencies_set = set( dependencies or () )

        step_instances = ( isinstance(dep, Step0) for dep in dependencies_set )
        if not all(step_instances):
            raise TypeError(
                       'dependencies argument must only include Step instances')
        self.dependencies = dependencies_set

        super().__init__(identifier, *args, **kwargs)

        self.logger.debug(self)

    def __hash__(self):
        return hash(self.class_id + self.id)

    def __eq__(self, other):
        return self.class_id == other.class_id and self.id == other.id

    def __str__(self):
        return '; '.join( (
            repr(self),
            f'dependencies: {self.dependencies}',
            f'simulation: {self.parent()}',
        ) )

    # @property
    # def dependencies(self):
    #     return self.__dependencies

    def execute(self, context=None, conf=None):
        self.logger.debug("Step %s (conf: %s, context: %s).", repr(self),
                                                             self.conf, context)

    # TODO
    def setup(self):
        pass

    # TODO - to use with context manager
    def initialize(self):
        pass

    # TODO - to use with context manager
    def finalize(self):
        pass


class Stage1(Part0):
    """Base class for concrete stage implementations

    The purpose of this class is to serve as the base of specific :class:`Stage`
    implementations, performing model specific stuff.

    Warning
    -------
    Concrete classes **must implement** the :meth:`execute` method.

    See also
    --------
    engine.core.parts.stage.Layer1 :
        extends this class by adding a finalize method
    """

    transient = False

    def execute(self):
        """stage's execution entry point"""
        raise NotImplementedError


class Layer1(Stage1):
    """Base class for concrete layer implementations

    The purpose of this class is to serve as the base of specific
    :class:`Layer` implementations, performing model specific stuff.

    **A layer is still a stage and treated like one.**
    :class:`Layer1` extends :class:`Stage1` by adding a :meth:`finalize` method,
    called after executing the stages (and layers).
    Layers behave as a wrapper for the stages (and layers) executed afterwards,
    :meth:`execute` acts as the entry and :meth:`finalize` the exit points.

    Warning
    -------
    Concrete classes **must implement** the methods :meth:`execute` and
    :meth:`finalize`.
    """

    def finalize(self):
        """layer's finalize entry point"""
        raise NotImplementedError
