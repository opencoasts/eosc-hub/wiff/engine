from enum import Enum


class CommonName:

    PATH = 'path'
    DATE_TIME = 'date_time'
    SIMULATION_KIND = 'simulation_kind'

    FORCINGS = 'forcings'

    class ForcingKind:

        OCEAN_ELEV = 'ocean_elev'
        OCEAN_SALT_TEMP = 'ocean_salt_temp'
        ATMOSPHERIC = 'atmospheric'

cn = CommonName


class ForcingSource:

    NO_FORCING = 'no_forcing'
    INPUT_VALUE = 'input_value'

    PRISM_2017 = 'prism_2017'
    FES_2014 = 'fes_2014'
    CMEMS_GLOBAL = 'cmems_global'
    CMEMS_IBI = 'cmems_ibi'

    WAVE_WATCH_3 = 'ww3'

    MARTEC_SINERGIA_REGIONAL = 'martec_sinergia_regional'
    MARTEC_SINERGIA_ALBUFEIRA = 'martec_sinergia_albufeira'
    METEOFRANCE_ARGPEGE_EUROPE = 'meteofrance_arpege_europe'
    METEOFRANCE_ARGPEGE_GLOBAL = 'meteofrance_arpege_global'
    METEOGALICIA_WRF_WEST_EUROPE = 'meteogalicia_wrf_west_europe'
    METEOGALICIA_WRF_IBERIA_BISCAY = 'meteogalicia_wrf_iberia_biscay'
    METEOGALICIA_WRF_GALICIA = 'meteogalicia_wrf_galicia'
    NOAA_GFS_0P25 = 'noaa_gfs_0p25'
    NOAA_NAM = 'noaa_nam'
    NOAA_NAM_CONUS = 'noaa_nam_conus'


class ForcingKind:

    TEMPERATURE = 'temperature' # legacy
    SALINITY = 'salinity' # legacy

    ELEVATION = 'elevation'
    OCEAN_TEMPERATURE = 'ocean_temperature'
    OCEAN_SALINITY = 'ocean_salinity'
    WAVES = 'waves'

    FLOW = 'flow'
    RIVER_TEMPERATURE = 'river_temperature'
    RIVER_SALINITY = 'river_salinity'

    AIR = 'air'
    OPTIONAL_AIR = 'optional_air' # makes sense to exist?
    RADIATION = 'radiation'

    fs = ForcingSource
    air_common = (
        fs.MARTEC_SINERGIA_REGIONAL,
        fs.MARTEC_SINERGIA_ALBUFEIRA,
        fs.METEOFRANCE_ARGPEGE_EUROPE,
        fs.METEOFRANCE_ARGPEGE_GLOBAL,
        fs.METEOGALICIA_WRF_WEST_EUROPE,
        fs.METEOGALICIA_WRF_IBERIA_BISCAY,
        fs.METEOGALICIA_WRF_GALICIA,
        fs.NOAA_GFS_0P25,
        fs.NOAA_NAM,
        fs.NOAA_NAM_CONUS,
    )

    SOURCE_MAP = {
        ELEVATION: (fs.PRISM_2017, fs.FES_2014, fs.CMEMS_GLOBAL, fs.CMEMS_IBI),
        OCEAN_TEMPERATURE: (fs.CMEMS_GLOBAL, fs.CMEMS_IBI),
        OCEAN_SALINITY: (fs.CMEMS_GLOBAL, fs.CMEMS_IBI),
        WAVES: (fs.WAVE_WATCH_3, ),

        FLOW: (fs.INPUT_VALUE, ),
        RIVER_TEMPERATURE: (fs.INPUT_VALUE, ),
        RIVER_SALINITY: (fs.INPUT_VALUE, ),

        AIR: air_common,
        OPTIONAL_AIR: (fs.NO_FORCING, *air_common),
        RADIATION: air_common,
    }

class ForcingSeriesKind:

    ANNUAL = 'annual'
    RATIO = 'ratio'
    TH_SERVICE_URL = 'th_service_url'


class BoundaryKind:

    OCEAN = 'ocean'
    RIVER = 'river'
    ATMOSPHERIC = 'atmospheric'


class ConfigurationKind:

    PARAM_IN = 'param_in'
    WWMINPUT_NML = 'wwminput_nml'


class SimulationKind:

    BAROTROPIC = 'barotropic'
    BAROTROPIC_WAVES = 'barotropic_waves'
    BAROCLINIC = 'baroclinic'

    bk = BoundaryKind
    fk = ForcingKind

    FORCINGS_MAP = {
        BAROTROPIC: {
            bk.OCEAN: (fk.ELEVATION, ),
            bk.RIVER: (fk.FLOW, ),
            bk.ATMOSPHERIC: (fk.OPTIONAL_AIR, ),
        },
        BAROTROPIC_WAVES: {
            bk.OCEAN: (fk.ELEVATION, fk.WAVES),
            bk.RIVER: (fk.FLOW, ),
            bk.ATMOSPHERIC: (fk.OPTIONAL_AIR, ),
        },
        BAROCLINIC: {
            bk.OCEAN: (fk.ELEVATION, fk.OCEAN_TEMPERATURE, fk.OCEAN_SALINITY),
            bk.RIVER: (fk.FLOW, fk.TEMPERATURE, fk.SALINITY), # reversed
            bk.ATMOSPHERIC: (fk.AIR, fk.RADIATION),
        },
    }

    ck = ConfigurationKind

    CONFIGURATION_MAP = {
        BAROTROPIC: (ck.PARAM_IN, ),
        BAROTROPIC_WAVES: (ck.PARAM_IN, ck.WWMINPUT_NML),
        BAROCLINIC: (ck.PARAM_IN, ),
    }


class StationInOutputKind(Enum):

    ELEVATION = 1
    AIR_PRESSURE = 2
    WIND_U = 3
    WIND_V = 4
    TEMPERATURE = 5
    SALINITY = 6
    VELOCITY_U = 7
    VELOCITY_V = 8
    W = 9
