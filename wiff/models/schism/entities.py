import re
from datetime import timedelta
from pathlib import Path


class ParamIn:

    class Parameter:

        TIMESTEP = 'dt'
        HOTSTART_FLAG = 'ihot'


    PARAM_REGEX_TEMPLATE = (
         r'(?P<pre_value>^\s* \{param\} \s*=\s* )'
         r'(?P<value>.+?)'
         r'(?P<post_value> \s*(?:!.*)?$)'
    )

    def __init__(self, base, *, timestep=None, hotstarted=None, params=None):
        self._base = base
        self._param_in = base
        self._params = params or dict()

        if timestep:
            self.timestep = timestep

        if hotstarted != None:
            self.hotstarted = hotstarted

    @property
    def base(self):
        return self._base

    @property
    def timestep(self):
        try:
            return self._timestep
        except AttributeError:
            timestep_secs = int( self.get_param(self.Parameter.TIMESTEP) )
            return timedelta(seconds=timestep_secs)

    @timestep.setter
    def set_timestep(self, timedelta):
        self.set_param(self.Parameter.TIMESTEP, timedelta.total_seconds() )
        # self.set_param(Parameter., ) # remaining dependencies
        self._timestep = timedelta

    @property
    def hotstarted(self):
        try:
            return self._hotstarted
        except AttributeError:
            return int( self.get_param(self.Parameter.HOTSTART_FLAG) )

    @hotstarted.setter
    def set_hotstarted(self, is_hotstarted):
        self.set_param(self.Parameter.HOTSTART_FLAG, 2 if hotstart else 0)
        self._hotstarted = is_hotstarted

    @classmethod
    def from_file(cls, path, **kwargs):
        base = Path(path).read_text()
        return cls(base, **kwargs)

    def _change_param_in(self, param, value):
        param_re = self.PARAM_REGEX_TEMPLATE.format(param=param)
        replace = rf'\g<pre_value>{value}\g<post_value>'
        self._param_in, changes = re.subn(param_re, replace, self._param_in,
                           flags=re.ASCII|re.IGNORECASE|re.MULTILINE|re.VERBOSE)

        return changes

    def get_param(self, param):
        return self._params.get(param) or self.base_param(param)

    def set_param(self, param, value):
        changes = self._change_param_in(param, value)
        if changes:
            self._params[param] = value

        return changes

    def base_param(self, param):
        param_re = self.PARAM_REGEX_TEMPLATE.format(param=param)
        re_matches = re.search(param_re, self.base,
                           flags=re.ASCII|re.IGNORECASE|re.MULTILINE|re.VERBOSE)

        return re_matches['value']

    def as_string(self):
        return self._param_in

    def save_as_file(self, path):
        Path(path).write_text(self._param_in)
        print(f"param.in saved as {path}")


class BctidesIn:

    class ForcingFlag:

        NONE = 0
        UNIFORM_SERIES_FILE = 1
        CONSTANT_VALUE = 2
        DEFINITION_LINES = 3
        SPATIAL_SERIES_FILE = 4
        DEF_LINES_N_SPATIAL_SERIES_FILE = 5

    class ForcingKind:

        ELEVATION = 'elev'
        VELOCITY = 'vel'
        TEMPERATURE = 'temp'
        SALINITY = 'salt'


    def __init__(self, open_boundaries, header='', tidal_potential=None,
                       tp_cut_off_depth=.0, tidal_potential_at_boundaries=None):

        self.open_boundaries = open_boundaries

        self.header = header
        self.tidal_potential = tidal_potential or list()
        self.tp_cut_off_depth = tp_cut_off_depth
        self.tidal_potential_at_boundaries = tidal_potential_at_boundaries or \
                                                                          list()

    @classmethod
    def _parse(cls, text):
        text_iter = iter( text.strip().splitlines() )
        next_line = lambda: next(text_iter)

        header = next_line()

        tidal_potential_size, tp_cut_off_depth, _ = cls._parse_line(
                                                                   next_line() )
        tidal_potential = cls._parse_tidal_potential(text_iter,
                                                     int(tidal_potential_size) )

        tidal_potential_at_boundaries_size, _ = cls._parse_line( next_line() )
        tidal_potential_at_boundaries = cls._parse_tidal_potential(text_iter,
                                       int(tidal_potential_at_boundaries_size) )

        return dict(
            header = header,
            tidal_potential = tidal_potential,
            tp_cut_off_depth = tp_cut_off_depth,
            tidal_potential_at_boundaries = tidal_potential_at_boundaries,
            open_boundaries = cls._parse_open_boundaries(text_iter,
                                            tidal_potential_at_boundaries_size),
        )

    @classmethod
    def _parse_line(cls, line):
        values, separator, comments = line.partition('#')
        return (*values.split(), comments)

    @classmethod
    def _parse_tidal_potential(cls, line_iterator, constituent_size):
        next_line = lambda: next(line_iterator)
        return tuple( f'{next_line()}\n\t{next_line()}'
                                              for _ in range(constituent_size) )

    @classmethod
    def _parse_open_boundaries(cls, line_iterator, tp_at_bnd_size=0,
                                                           optional_flags=None):
        next_line = lambda: next(line_iterator)
        next_lines = lambda count: ( next_lines() for _ in range(count) )

        open_boundaries_size, _ = cls._parse_line( next_line() )

        open_boundaries_definition = list()
        for _ in range( int(open_boundaries_size) ):
            definition = list()
            include_next_line = lambda: definition.append( next_line() )
            include_next_lines = lambda count: definition.extend(
                                                             next_lines(count) )

            specs_line = next_line()
            definition.append(specs_line)

            (node_count, elev_flag, vel_flag, temp_flag, salt_flag, *opt_flags,
                                                _) = cls._parse_line(specs_line)

            # elevation
            if int(elev_flag) == cls.ForcingFlag.CONSTANT_VALUE:
                include_next_line()

            elif int(elev_flag) == cls.ForcingFlag.DEFINITION_LINES:
                for _ in range(tp_at_bnd_size):
                    include_next_lines( 1 + int(node_count) )

            # velocity
            if int(vel_flag) == cls.ForcingFlag.CONSTANT_VALUE:
                include_next_line()


class MirrorOut:
    pass


def _main_paramin(base_path, param_in_path):
    params = dict(
    )
    param_in = ParamIn.from_file(Path(base_path), **params)

    def get_n_print(param):
        print( param_in.base_param(param) )

    #get_n_print('ics')
    #param_in.save_as_file( Path(param_in_path) )
    param_in.set_param('ics', 3)
    param_in.set_param('ipre', 1)
    print(param_in.as_string())

_main_func = _main_paramin

if __name__ == '__main__':
    from sys import argv
    main_func(*argv[1:])
