from datetime import datetime, date, time, timedelta
from pathlib import Path

from wiff.engine.parts import Stage
from wiff.engine.mixins.file_handler import FileHandler
from wiff.engine.mixins.process_handler import ProcessHandler


class SCHISMStage(Stage, FileHandler, ProcessHandler):

    datetime_today = lambda: datetime.combine(date.today(), time() )
    default_period = timedelta(hours=48)

    path = Stage.Input(Path.cwd)
    date_time = Stage.Input(datetime_today)
    period = Stage.Input(default_period, alias='simulation_period')

    series_interval = Stage.Input(optional=True)

    hotstart_interval = timedelta(seconds=3600)
