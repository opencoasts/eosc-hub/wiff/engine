from itertools import chain

from .base import SCHISMStage


class Cleanup0(SCHISMStage):

    xz_bin = SCHISMStage.Input(default='/usr/bin/xz')
    tar_bin = SCHISMStage.Input(default='/usr/bin/tar')
    do_remove = SCHISMStage.Input(default=True)
    do_compress = SCHISMStage.Input(default=True)
    do_archive = SCHISMStage.Input(default=True)

    remove_list = (
        'inputs.tar.gz',
        'outputs.tar.gz',

        '*.bin',
        'fort.33',
        'fort.5002',

        'hotstart.in',
        'wwm_hotstart.nc',

        'outputs/?_00??_*.6?',
        'outputs/*_00??_hotstart',

        # 'outputs/?_*.6?',
        'outputs/?_????.nc',
        'outputs/?_wwm_?.nc',

        'outputs/local_to_global_00??',
        'outputs/global_to_local.prop',
        'outputs/nonfatal_00??',
        'outputs/maxdahv_00??',
        'outputs/maxelev_00??',
        'outputs/windbg*_00??',
        'outputs/wwmdbg*_00??',
        'outputs/wwmstat*_00??',
        'outputs/combine_output.in',
        'outputs/fort.98',
    )

    compress_list = (
        'outputs/?_*.6?',
        'outputs/*_hotstart.in',
        'outputs/wwm_hotstart.nc',
    )
    compress_xz_args = '--compress --threads=1 --verbose'.split()

    archive_list = (
        ('fort.11 mirror.out', 'logs.tar.xz'),
        ('param.in wwminput.nml', 'confs.tar.xz'),
        ('bctides.in sflux/ ww3_spec.nc *D.th', 'forcings.tar.xz'),
    )
    archive_tar_args = '--create --auto-compress --verbose'.split()
                   # '--create --auto-compress --remove-files --verbose'.split()

    def execute(self):
        if self.do_remove:
            self.remove()

        if self.do_compress:
            self.compress()

        if self.do_archive:
            self.archive()

    def remove(self):

        def is_recursive_(file_pattern):
            is_recursive = '**' in file_pattern

            if is_recursive:
                self.logger.warning(
                    '** recursive patterns are no allowed and will be skipped!')

            return is_recursive

        files_to_remove = filter(
            lambda file: not file.is_symlink(),
                    chain.from_iterable( map(self.path.glob, self.remove_list) )
        )

        return tuple( map(self.remove_file, files_to_remove) )

    def compress(self):

        for compress_pattern in self.compress_list:
            files_to_compress = tuple( self.path.glob(compress_pattern) )

            if not files_to_compress:
                self.logger.debug('nothing to compress in %s', compress_pattern)

            else:
                self.logger.info('compressing %s', compress_pattern)
                self.run_executable(self.xz_bin, *self.compress_xz_args,
                                                             *files_to_compress)

    def archive(self):

        for archive_patterns, archive_file in self.archive_list:
            files_to_archive = tuple( chain.from_iterable(
                              map(self.path.glob, archive_patterns.split() ) ) )
            relative_files_to_archive = tuple( file_path.relative_to(self.path)
                                             for file_path in files_to_archive )

            if not relative_files_to_archive:
                self.logger.debug('nothing to archive in %s', archive_patterns)

            else:
                self.logger.info('archiving %s into %s', archive_patterns,
                                                                   archive_file)
                self.run_executable(self.tar_bin, *self.archive_tar_args,
                           f'--file={archive_file}', *relative_files_to_archive)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)

    from sys import argv
    path, = argv[1:]

    from pathlib import Path
    Cleanup0('test', path=Path(path)).execute()