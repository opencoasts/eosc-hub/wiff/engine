import re
from datetime import datetime, timedelta
from subprocess import TimeoutExpired

from .base import SCHISMStage


class Compute0(SCHISMStage):

    timestep = SCHISMStage.Input()
    schism_executable = SCHISMStage.Input('schism')

    def execute(self):
        self.run_schism()
        self.check_mirror_out_file()

    def run_schism(self):
        try:
            self.create_folder('outputs')
        except FileExistsError:
            self.logger.debug("schism outputs dir already exists")

        # schism_process = Process(
        #     target=self.run_executable_mpi,
        #     args=(schism_bin, ),
        #     kwargs=dict(proc_count=self.process_count, cwd=self.path,
        #                                                           verbose=True),
        #     daemon=True,
        # )
        # schism_process.start()

        schism_path = self.path / self.schism_executable
        schism_process = self.run_executable_mpi(schism_path,
                                 cwd=self.path, sync=False, release_queue=False)

        # should it act on except (captures exception) or finally
        try:
            self._monitor_schism_execution(schism_process)
        except:
            self.logger.exception("monitoring schism execution went wrong!")
            schism_process.kill()
        finally:
            self.mpi_queue.release()

    _mirror_out_time_step_regex = \
                             re.compile(r'TIME STEP=\s*(?P<step>\d+);  TIME=.*')

    schism_monitoring_settle = timedelta(seconds=60)
    schism_monitoring_interval = timedelta(minutes=5)

    # integrate a similir mechanism into execute (monitor_fn)
    def _monitor_schism_execution(self, schism_process):

        mirror_out_path = self.path / 'mirror.out'

        settle_secs = self.schism_monitoring_settle.total_seconds()
        self.logger.debug('waiting %d seconds for schism process to settle',
                                                                    settle_secs)

        try:
            schism_process.wait(settle_secs)

        except TimeoutExpired:
            refresh_secs = self.schism_monitoring_interval.total_seconds()
            self.logger.debug('schism progress will be monitored, '
                  'checking %s every %i seconds', mirror_out_path, refresh_secs)

        else:
            # self.logger.warning('schism terminated rather quickly: \n > %s',
            #                                '\n > '.join(schism_process.stdout) )
            proc_stdout, _ = schism_process.communicate() # TODO: parse properly
            self.logger.warning('schism terminated rather quickly:\n%s',
                                                                    proc_stdout)
            return

        current_steps, delta_steps = 0, None

        def parse_progress_from_mirror_out():
            nonlocal current_steps, delta_steps

            previous_steps = current_steps
            last_mirror_out_lines = \
                 mirror_out_path.read_text(errors='replace')[-100:].splitlines()

            for line in reversed(last_mirror_out_lines):

                if line.startswith('TIME STEP='):
                    regex_fullmatch = \
                                self._mirror_out_time_step_regex.fullmatch(line)

                    if regex_fullmatch:
                        current_steps = int( regex_fullmatch.group('step') )
                        delta_steps = current_steps - previous_steps

                else:
                    break

        total_steps = self.period // self.timestep

        def progress_message(interval=self.schism_monitoring_interval):
            parse_progress_from_mirror_out()

            if not delta_steps:
                return 'not enough progress yet'

            remain_steps = total_steps - current_steps
            estimated_remain_time = remain_steps / delta_steps * interval
            estimated_end_time = datetime.now() + estimated_remain_time
            # self.logger.debug(
            #     "STEPS delta=%d remain=%s ESTIMATED TIME remain=%s end=%s",
            #      delta_steps, remain_steps, estimated_remain_time, 
            #                                                   estimated_end_time
            # )

            execution_ratio = current_steps / total_steps
            return ( f'{execution_ratio:.2}={current_steps}/{total_steps}; '
                                    f'eta {estimated_end_time:%Y/%m/%d %H:%M}' )

        self.logger.debug('schism execution progress: %s',
                               progress_message(self.schism_monitoring_settle) )

        previous_mtime = None
        while schism_process.poll() is None:

            try:
                schism_process.wait(refresh_secs)

            except TimeoutExpired:
                self.logger.debug('schism execution progress: %s',
                                                            progress_message() )

                try:
                    mtime = mirror_out_path.stat().st_mtime
                except FileNotFoundError:
                    mtime = previous_mtime

                if mtime == previous_mtime:
                    self.logger.debug('schism process stalled! killing it...')
                    schism_process.kill()

                previous_mtime = mtime

        self.logger.info("schism execution finished")

    def check_mirror_out_file(self):
        mirror_out_path = self.path / 'mirror.out'

        try:
            mirror_out_text = mirror_out_path.read_text()
        except FileNotFoundError:
            pass
        else:
            if 'Run completed successfully' in mirror_out_text:
                self.logger.info('simulation run completed successfully!')
                return

        raise ValueError('simulation run failed!')

        # was_hotstarted = 'hot start at time' in mirror_out_text


from rsub.client import SProxyExecution, make_archive_tar_gz, \
                                                          extract_archive_tar_gz

from ..common import SimulationKind


class SProxyCompute0(SCHISMStage):

    simulation_kind = SCHISMStage.Input()
    url = SCHISMStage.Input()
    blob_service = SCHISMStage.Input()
    job_service = SCHISMStage.Input()
    label = SCHISMStage.Input()

    timestep = SCHISMStage.Input() # temporary

    def execute(self):
        self.pack_inputs()
        self.submit_schism()
        self.unpack_outputs()
        #Compute.check_mirror_out_file(self)
        self.check_mirror_out_file()

    input_files = (
        'albedo.gr3',
        'bctides.in',
        'diffmax.gr3',
        'diffmin.gr3',
        'drag.gr3',
        'elev2D.th',
        'hgrid.gr3',
        'hgrid.ll',
        'hotstart.in',
        'manning.gr3',
        'outputs/',
        'param.in',
        'SAL_3D.th',
        'salt.ic',
        'sflux/',
        'TEM_3D.th',
        'temp.ic',
        'uv3D.th',
        'vgrid.in',
        'watertype.gr3',
        'windrot_geo2proj.gr3',
        'ww3_spec.nc',
        'wwmbnd.gr3',
        'wwm_hotstart.nc',
        'wwminput.nml',
    )
    inputs_map = dict(
        inputs = 'inputs.tar.gz',
    )

    def pack_inputs(self):
        outputs_path = self.path / 'outputs'
        outputs_path.mkdir(exist_ok=True)

        inputs_tar_gz_path = self.path / self.inputs_map['inputs']
        make_archive_tar_gz(inputs_tar_gz_path, self.input_files)

    simulation_kind_exec_map = {
        SimulationKind.BAROTROPIC: 'schism-barotropic',
        SimulationKind.BAROTROPIC_WAVES: 'schism-barotropic_waves',
        SimulationKind.BAROCLINIC: 'schism-baroclinic',
    }

    def submit_schism(self):
        sproxy_exec = SProxyExecution(
            url = self.url,
            blob_service = self.blob_service,
            job_service = self.job_service,
            label = self.label,
            work_dir = self.path,
            arguments = dict(
                exec = self.simulation_kind_exec_map[self.simulation_kind],
            ),
            inputs_map = self.inputs_map,
            outputs_map = self.outputs_map,
            keep_blobs = False,
        )
        try:
            sproxy_exec.prepare().submit().wait().fetch()
        finally:
            sproxy_exec.clean()

    outputs_map = dict(
        # mirror = 'mirror.out',
        outputs = 'outputs.tar.gz',
        sandbox = 'sandbox.tar.gz',
    )

    def unpack_outputs(self):
        outputs_tar_gz_path = self.path / self.outputs_map['outputs']
        extract_archive_tar_gz(outputs_tar_gz_path)

    check_mirror_out_file = Compute0.check_mirror_out_file


class StateMachine:

    states = ('inited', )

    def __init__(self, *args, initial_state=None, **kwargs):
        self.state = initial_state or self.states[0]

        super.__init__(*args, **kwargs)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        if value in self.states:
            self._state = value
        else:
            raise ValueError( "invalid state, must be: "
                                                        ', '.join(self.states) )

    def _assert_state(*required_states,
                                   error_message='assertion failed, expecting'):

        if not self.state in required_states:
            return

        required_states_str = ', '.join(required_states)
        raise AssertionError(f"{error_message} {required_states_str}"
                                                          f", not {self.state}")


class ExecutionOffloading(StateMachine):
    """Abstract class to model how execution on third party systems should be
    exposed"""

    states = ('inited', 'prepared', 'started', 'paused', 'stopped', 'completed',
                                                                     'finished')

    def __init__(self, *, prepare=False):
        super.__init__()

        if prepare:
            self._prepare()

    def _prepare(self):
        """[private] method to prepare everything needed to execute"""
        self.state = 'prepared'

    def start(self):
        """[public] method to start the exection"""
        self._assert_state('prepared', error_message="can only start from")

        self.state = 'started'

    def _monitor(self):
        """[private] method to monitor the execution"""
        self._assert_state('started', error_message="can only monitor if")

        self._last_update = datetime.now()

    def status(self):
        """[public] method to get the exection's status"""
        self._assert_state('started', error_message="can only get status if")

    def pause(self):
        """[public] method to pause a started exection"""
        self._assert_state('started', error_message="can only pause if")

        self.state = 'paused'

    def resume(self):
        """[public] method to resume a paused exection"""
        self._assert_state('paused', error_message="can only resume if")

        self.state = 'started'

    def stop(self):
        """[public] method to stop/abort/cancel the exection"""
        self._assert_state('started', 'paused',
                                               error_message="can only stop if")

        self.state = 'stopped'

    def _finish(self):
        """[private] method to finish everything after the execution"""
        self._assert_state('stop', error_message="can only finish if")

        self.state = 'finished'

    def has_started(self):
        """[public] method to query if th   e exection has started"""
        pass

    def has_finished(self):
        """[public] method to query if the exection has finished"""
        pass

    def wait_until_finished(self):
        """[public] method to block until the exection has finished"""
        pass
