from .base import SCHISMStage


class Configure0(SCHISMStage):

    param_in = SCHISMStage.Input(dict)

    def execute(self):
        self._format_param_in()

    def _format_param_in(self):
        template_path = self.path / 'param.in.template'
        formatted = template_path.read_text().format_map(self.param_in)
        self.create_text_file('param.in', formatted)

        self.logger.info('formatted param.in from %s', template_path)