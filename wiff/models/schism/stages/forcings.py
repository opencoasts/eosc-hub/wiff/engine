import json
import os.path
from collections import ChainMap
from datetime import date, time, timedelta
from functools import partial
from pathlib import Path

from wiff.forcings.providers.copernicus.cmems import CMEMS as copernicus_CMEMS
from wiff.forcings.providers.meteofrance.dcpc import DCPC as meteofrance_DCPC
from wiff.forcings.providers.meteogalicia.mandeo import Mandeo as \
                                                             meteogalicia_Mandeo
from wiff.forcings.providers.noaa.nomads import NOMADS as noaa_NOMADS
from wiff.forcings.providers.tecnico.martec import MARTEC as tecnico_MARTEC

from wiff.core import utils
#engine.mixins import FileHandler, ProcessHandler

from .base import SCHISMStage
from ..common import CommonName, SimulationKind, BoundaryKind, ForcingSource, \
                                                                     ForcingKind


class Forcings0(SCHISMStage):

    simulation_kind = SCHISMStage.Input()
    boundaries = SCHISMStage.Input()
    forcings = SCHISMStage.Input()
    bbox = SCHISMStage.Input()

    param_in = SCHISMStage.Input(dict, output=True)
    vertical_levels_count = SCHISMStage.Input(optional=True)

    cache_path = SCHISMStage.Input(optional=True)
    fes2014_exec = SCHISMStage.Input(default='fes2014.sh')
    motuclient_exec = SCHISMStage.Input(default='motuclient')
    motuclient_config = SCHISMStage.Input(optional=True)
    martec_ftp_login = SCHISMStage.Input(dict)

    #ocean_boundaries_files = SCHISMStage.Output()

    def execute(self):
        self.timestep = timedelta(seconds=self.param_in['dt'])

        self.resolve_ocean_boundary_elev_forcings()
        if self.simulation_kind == SimulationKind.BAROCLINIC:
            self.resolve_ocean_boundary_salt_temp_forcings()

        self._resolve_atmospheric_forcings()

        self.format_bctides_in()


    class CMEMSDomain:

        GLOBAL = 'global'
        IBI = 'ibi'


    # TODO: add explicit no_forcings and disallow KeyError exception
    def resolve_ocean_boundary_elev_forcings(self):
        forcing_resolver_map = {
            ForcingSource.PRISM_2017:
                              self._resolve_ocean_boundary_forcings_using_prism,
            ForcingSource.FES_2014:
                            self._resolve_ocean_boundary_forcings_using_fes2014,
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                       self.CMEMSDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                          self.CMEMSDomain.IBI),
        }

        try:
            forcing = self.forcings[CommonName.ForcingKind.OCEAN_ELEV]
        except KeyError:
            self.logger.info('No ocean elevation forcing defined!')
        else:
            forcing_resolver_map[forcing]()

    # TODO: add explicit no_forcings and disallow KeyError exception
    def resolve_ocean_boundary_salt_temp_forcings(self):
        forcing_resolver_map = {
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                       self.CMEMSDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                          self.CMEMSDomain.IBI),
        }

        try:
            forcing = self.forcings[CommonName.ForcingKind.OCEAN_SALT_TEMP]
        except KeyError:
            pass
        else:
            forcing_resolver_map[forcing]()

    prism_dir = 'prism'

    @SCHISMStage.cache_files('elev2D.th')
    def _resolve_ocean_boundary_forcings_using_prism(self):

        current_date = self.date_time.date()
        previous_date = current_date - timedelta(days=1)

        prism_file = lambda date, filename: f'{date:%Y-%j}/run/{filename}'
        prism_nesting_files = (
            ( '1_elev.61', prism_file(previous_date, 'outputs/1_elev.61') ),
            ( '2_elev.61', prism_file(current_date, 'outputs/1_elev.61') ),
            ( '3_elev.61', prism_file(current_date, 'outputs/2_elev.61') ),
            ( 'bg.gr3', prism_file(current_date, 'hgrid.gr3') ),
        )

        for link_name, suffix_path in prism_nesting_files:

            prism_file_path = self.static_path / self.prism_dir / suffix_path
            self.link_file(prism_file_path, link_name)

        # the remaining funcionality was handled by genBCschism.sh script

        enumerated_boundaries = enumerate(self.boundaries, start=1)
        is_ocean_ = lambda boundary: boundary['kind'] == BoundaryKind.OCEAN

        boundaries_index, boundaries_node_count = zip( *(
            (index, boundary['node_count']) for index, boundary
                                 in enumerated_boundaries if is_ocean_(boundary)
        ) )

        boundary_count = len(boundaries_index)
        boundaries_indexes_listing = utils.listing(boundaries_index)

        gen_fg_in = utils.format_lines(
            '{boundary_count}'
                  ' !total # of open boundary segments to be included in fg.bp',
            '{boundaries_index} !list of open boundary segment IDs',

            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
        )
        self.create_text_file(self.path/'gen_fg.in', gen_fg_in)
        self.run_executable('gen_fg')

        self.link_file('hgrid.ll', 'fg.gr3')
        self.link_file('vgrid.in', 'vgrid.fg')

        period_days = utils.days_in_(self.period)
        interval_days = utils.days_in_(self.series_interval)
        period_extended_days = period_days + interval_days

        interpolate_variables_in_file_type_dict = dict(
            elev2D = 1,
            salt3D = 2,
            temp3D = 2,
            uv3D = 3,
        )
        interpolate_variables_in = utils.format_lines(
            '{file_type} {length_days} ! file_type: {file_type_dict}; days',
            '{boundary_count} {boundaries_index}'
                                          ' ! boundary count; boundary indexes',
            '{debug:0} ! debug: 0=off, 1=on',

            length_days = period_extended_days,
            file_type = interpolate_variables_in_file_type_dict['elev2D'],
            file_type_dict = interpolate_variables_in_file_type_dict,
            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
            debug = False,
        )
        self.create_text_file(self.path/'interpolate_variables.in',
                                                       interpolate_variables_in)
        self.run_executable('interpolate_variables6')

        self.move_file('elev2D.th', 'th.old')

        header_template = utils.format_lines(
            '{{length_days}} 1 ! length days, number of vertical levels',
            '{boundary_count} {boundaries_node_count}'
                              ' ! ocean boundaries, node count in each of them',

            boundary_count = boundary_count,
            boundaries_node_count = utils.listing(boundaries_node_count),
        )

        slash_th_in = utils.format_lines(
            header_template.format(length_days=period_extended_days),
            '{interval_days} !number of days to slash from the beginning',

            interval_days = interval_days
        )
        self.create_text_file(self.path/'slash_th.in', slash_th_in)

        self.run_executable('slash_th')

        timeint_in = utils.format_lines(
            header_template.format(length_days=period_days),
            '{timestep} ! timestep',
            # '{vertical_offset} ! PRISM vertical offset',

            timestep = int( self.timestep.total_seconds() ),
            # vertical_offset = context.get('vertical_offset') or 0.
        )
        self.create_text_file(self.path/'timeint.in', timeint_in)

        #self.rename_local_file(path, 'th.new', 'th.old')

        self.run_executable('timeint_th', 'th.new', 'elev2D.th')

        #self.rename_local_file(path, 'th.new', 'elev2D.th')

        #self.ocean_boundaries_files = prism_nesting_files

    @SCHISMStage.cache_files('bctides.json', 'uv3D.th')
    def _resolve_ocean_boundary_forcings_using_fes2014(self):

        def formatted_boundaries():
            for boundary in self.boundaries:
                is_ocean = boundary['kind'] == 'ocean'
                flow_details = "" if is_ocean else f":{boundary['flow']}"
                yield f"{boundary['kind']}{flow_details}"

        time_arg_fmt = '%Y-%m-%dT%H:%M:%SZ'
        end_date_time = self.date_time + self.period

        script_args = (
            #'-conf', self.static_path / 'slev_fes2014.ini',
            '--tstart', self.date_time.strftime(time_arg_fmt),
            '--tend', end_date_time.strftime(time_arg_fmt),
            #'--deltaT', self.timestep.total_seconds(),
            '--type', 'gr3',
            '--bnd', 'hgrid.ll',
            '--bnds_specs', *formatted_boundaries(),
            '--levels', str( self.vertical_levels_count or 2 ),
            '--velocities',
            '--out', 'bctides.in-fes2014',
            '--json'
        )

        self.run_executable(self.fes2014_exec, *script_args)

    @SCHISMStage.cache_files('elev2D.th')
    def _resolve_ocean_boundary_elev_forcings_using_cmems(self,
                                                               regional_domain):
        cmems = self._cmems_forcings(regional_domain,
                                              CommonName.ForcingKind.OCEAN_ELEV)
        cmems.create_elev2D_th(
            date=self.date_time.date(),
            output_dir=self.path,
        )

    @SCHISMStage.cache_files('SAL_3D.th', 'TEM_3D.th')
    def _resolve_ocean_boundary_salt_temp_forcings_using_cmems(self,
                                                               regional_domain):
        cmems = self._cmems_forcings(regional_domain,
                                         CommonName.ForcingKind.OCEAN_SALT_TEMP)

        create_args = dict(
            date=self.date_time.date(),
            output_dir=self.path,
        )
        cmems.create_SAL_3D_th(**create_args)
        cmems.create_TEM_3D_th(**create_args)

    @utils.QueuedExecution('cmems')
    def _cmems_forcings(self, regional_domain, kind):
        domain_dataset_map = {
            self.CMEMSDomain.GLOBAL: {
                CommonName.ForcingKind.OCEAN_ELEV: 'global_afp_0p083_hourly',
                CommonName.ForcingKind.OCEAN_SALT_TEMP:
                                                     'global_afp_0p083_3d_daily'
            },
            self.CMEMSDomain.IBI: {
                CommonName.ForcingKind.OCEAN_ELEV: 'ibi_afp_0p028_hourly',
                CommonName.ForcingKind.OCEAN_SALT_TEMP: 'ibi_afp_0p028_3d_daily'
            },
        }

        dataset_kind_map = domain_dataset_map[regional_domain]

        optional_args = dict()
        if self.vertical_levels_count:
            optional_args['depth_levels'] = self.vertical_levels_count

        return copernicus_CMEMS(
            dataset_key = dataset_kind_map[kind],
            update = time(0),
            period = self.period,
            extent = self.bbox,

            boundaries = self.boundaries,
            timestep = self.timestep,

            **optional_args,

            utils_dir = self.bin_path,
            templates_dir = self.static_path,
            elev_offset_nc = self.static_path / 'GLO-MFC_001_024_mdt.nc',
            motuclient = self.motuclient_exec,
            config_file = self.motuclient_config,
        )

    def _atmospheric_forcings_resolver_kind_map(self):
        return {
            ForcingSource.NO_FORCING: None,

            ForcingSource.MARTEC_SINERGIA_REGIONAL: partial(tecnico_MARTEC,
                               'sinergea_fc_regional', **self.martec_ftp_login),
            ForcingSource.MARTEC_SINERGIA_ALBUFEIRA: partial(tecnico_MARTEC,
                              'sinergea_fc_albufeira', **self.martec_ftp_login),

            ForcingSource.METEOFRANCE_ARGPEGE_EUROPE:
                                     partial(meteofrance_DCPC, 'arpege_europe'),
            ForcingSource.METEOFRANCE_ARGPEGE_GLOBAL:
                                     partial(meteofrance_DCPC, 'arpege_global'),

            ForcingSource.METEOGALICIA_WRF_WEST_EUROPE:
                                    partial(meteogalicia_Mandeo, 'west_europe'),
            ForcingSource.METEOGALICIA_WRF_IBERIA_BISCAY:
                                  partial(meteogalicia_Mandeo, 'ibiria_biscay'),
            ForcingSource.METEOGALICIA_WRF_GALICIA:
                                        partial(meteogalicia_Mandeo, 'galicia'),

            ForcingSource.NOAA_GFS_0P25: partial(noaa_NOMADS, 'gfs_0p25_1hr'),
            ForcingSource.NOAA_NAM: partial(noaa_NOMADS, 'nam'),
            ForcingSource.NOAA_NAM_CONUS: partial(noaa_NOMADS, 'nam_conus'),
        }

    _radiation_requirement_simulation_kinds = {
        SimulationKind.BAROCLINIC,
    }

    _inverse_barometer_forcings = {
        ForcingSource.FES_2014,
    }

    def _resolve_atmospheric_forcings(self):
        name = self.forcings.get(CommonName.ForcingKind.ATMOSPHERIC)
        if not name:
            return

        resolver_kind = self._atmospheric_forcings_resolver_kind_map()[name]
        sflux_path = self.path / 'sflux'

        if resolver_kind:
            resolver = resolver_kind(
                date_time = self.date_time,
                work_path = sflux_path,
                period = self.period,
                extent = self.bbox,
            )
            self._create_sflux(sflux_path, resolver)

            ocean_elev_forcing = \
                            self.forcings.get(CommonName.ForcingKind.OCEAN_ELEV)
            is_inverse_barometer = \
                        ocean_elev_forcing in self._inverse_barometer_forcings

            self.param_in.update(
                inv_atm_bnd = int(is_inverse_barometer),
                nws = 2, # sflux usage
                wtiminc = resolver.dataset.time_resolution.total_seconds(),
            )


    @SCHISMStage.cache_files('sflux/')
    def _create_sflux(self, path, resolver):
        self.create_folder(path)

        try:
            resolver.create_sflux_air()

            if self.simulation_kind \
                            in self._radiation_requirement_simulation_kinds:
                resolver.create_sflux_rad()
            
            resolver.create_inputs_txt()
        finally:
            resolver.close()

    # TODO: should be a class on its own
    def format_bctides_in(self):
        bctides_json_path = self.path / 'bctides.json'
        bctides_json = json.loads( bctides_json_path.read_bytes() ) \
                                       if bctides_json_path.exists() else dict()

        #base_date = context['date_time'].strftime('%m/%d/%Y %H:%M:%S')
        base_date = self.date_time.strftime('%Y-%m-%d %H:%M:%S')

        tidal_potential_header = \
                       '{freq_count} 0 ! nfreq tidal pot., cut-off depth for tp'
        tidal_potential_freqs = bctides_json.get('Tidal Potential') or ()
        tidal_potential = utils.format_lines(
            tidal_potential_header,
            *tidal_potential_freqs,

            freq_count = len(tidal_potential_freqs)
        )

        nodal_factors_header = '{freq_count} ! nfreq at bounds'
        nodal_factors_freqs = bctides_json.get('Nodal Factors') or ()
        nodal_factors = utils.format_lines(
            nodal_factors_header,
            *nodal_factors_freqs,

            freq_count = len(nodal_factors_freqs)
        )

        # def bctides_json_bnd_(kind):
        #     try:
        #         return bctides_json['Boundaries'][kind]
        #     except KeyError:
        #         return len(boundaries) * ( tuple(), )

        bctides_json_bnd_ = lambda kind: (
            bctides_json['Boundaries'][kind] if bctides_json
                                        else len(self.boundaries) * ( tuple(), )
        )

        bnd_elevations = bctides_json_bnd_('Elevations')
        bnd_velocities = bctides_json_bnd_('Velocities')

        boundary_header = (
            '{{node_count}} {elev_flag} {vel_flag} {temp_flag} {salt_flag}'
                     ' !{{kind}}: node count, elev, vel, temp and salt bc flags'
        )

        ocean_bc_flags = lambda bnd_index: dict(
            elev_flag = 3 if bnd_elevations[bnd_index] else 4,
            vel_flag = 5 if bnd_velocities[bnd_index] else 0,
        )
        river_bc_flags = dict(
            elev_flag = 0,
            vel_flag = 2,
        )
        barotropic_bc_flags = dict(
            temp_flag = 0,
            salt_flag = 0,
        )
        barotropic_boundary_template = {
                BoundaryKind.OCEAN: lambda bnd_index: (
                    boundary_header.format(
                        **ocean_bc_flags(bnd_index),
                        **barotropic_bc_flags,
                    ),
                    *bnd_elevations[bnd_index],
                    *bnd_velocities[bnd_index],
                ),
                BoundaryKind.RIVER: lambda _: (
                    boundary_header.format(
                        **river_bc_flags,
                        **barotropic_bc_flags,
                    ),
                    f'{{{ForcingKind.FLOW}}} !flux',
                ),
            }

        simulation_kind__boundary_templates__map = {

            SimulationKind.BAROTROPIC: barotropic_boundary_template,

            SimulationKind.BAROTROPIC_WAVES: barotropic_boundary_template,

            SimulationKind.BAROCLINIC: {
                BoundaryKind.OCEAN: lambda bnd_index: (
                    boundary_header.format(
                        **ocean_bc_flags(bnd_index),
                        temp_flag = 4,
                        salt_flag = 4,
                    ),
                    *bnd_elevations[bnd_index],
                    *bnd_velocities[bnd_index],
                    '1. ! temperature (TEM_3D.th) nudging factor',
                    '1. ! salinity (SAL_3D.th) nudging factor',
                ),
                BoundaryKind.RIVER: lambda _: (
                    boundary_header.format(
                        **river_bc_flags,
                        temp_flag = 2,
                        salt_flag = 2,

                    ),
                    f'{{{ForcingKind.FLOW}}} ! flux',
                    f'{{{ForcingKind.TEMPERATURE}}} ! temperature',
                    '1. ! temperature nudging factor',
                    f'{{{ForcingKind.SALINITY}}} ! salinity',
                    '1. ! salinity nudging factor',
                ),
            },

        }
        boundary_templates = simulation_kind__boundary_templates__map[
                                                           self.simulation_kind]
        format_boundary = lambda index, details: '\n'.join(
              boundary_templates[ details['kind'] ](index) ).format_map(details)

        open_boundaries_header = '{bnd_count} ! open boundary segments'
        open_boundaries = utils.format_lines(
            open_boundaries_header,
            *( format_boundary(*enum_details)
                               for enum_details in enumerate(self.boundaries) ),

            bnd_count = len(self.boundaries)
        )

        bctides_in_template_path = self.static_path / 'bctides.in.template'
        bctides_in = utils.format_lines(
            bctides_in_template_path.read_text(),

            BASE_DATE = base_date,
            TIDAL_POTENTIAL = tidal_potential,
            NODAL_FACTORS = nodal_factors,
            OPEN_BOUNDARIES = open_boundaries,
        )
        bctides_in_path = self.path / 'bctides.in'
        bctides_in_path.write_text(bctides_in)

        self.logger.info('formatted bctides.in from template %r',
                                                       bctides_in_template_path)
