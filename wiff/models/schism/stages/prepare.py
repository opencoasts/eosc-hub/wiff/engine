import json
import os.path
from collections import ChainMap
from datetime import date, time, timedelta
from functools import partial
from pathlib import Path

from wiff.core import utils
#engine.mixins import FileHandler, ProcessHandler

from .base import SCHISMStage
from ..common import StationInOutputKind, ForcingSource


class Prepare0(SCHISMStage):

    simulation_kind = SCHISMStage.Input()
    boundaries = SCHISMStage.Input()
    forcings = SCHISMStage.Input()
    is_cartesian = SCHISMStage.Input(default=False)
    force_coldstart = SCHISMStage.Input(default=False)
    param_in = SCHISMStage.Input(default=dict)

    force_hotstart_path = SCHISMStage.Input(optional=True)
    vertical_levels_count = SCHISMStage.Input(optional=True)
    bbox = SCHISMStage.Input(optional=True) # TODO: determine it
    xz_bin = SCHISMStage.Input(default='/usr/bin/xz')
    fes2014_exec = SCHISMStage.Input(default='fes2014.sh')
    motuclient_exec = SCHISMStage.Input(default='motuclient')

    root_path = SCHISMStage.Input() # to remove

    timestep = SCHISMStage.Output()
    hotstart_path = SCHISMStage.Output()
    hotstart_datetime = SCHISMStage.Output()
    ocean_boundaries_files = SCHISMStage.Output()

    def __init__(self, name, **kwargs):
        self.base_logger.info( utils.class_mark(__class__) )

        super().__init__(name, **kwargs)

        utils.unfold_path(self.root_path, strict=True)
        self._update_filepaths()

    def set_parent(self, name, parent):
        super().set_parent(name, parent)
        # TODO: update filepaths and timmings

    def execute(self):
        self.timestep = timedelta( seconds=int(self.param_in['dt']) )

        self._resolve_simulation_mode()
        self._resolve_hotstart()
        self._populate_workspace()
        self._resolve_ocean_boundary_elev_forcings()
        if self.simulation_kind == SimulationKind.BAROCLINIC:
            self._resolve_ocean_boundary_salt_temp_forcings()
        self._resolve_atmospheric_forcings()
        self._format_bctides_in()
        self._format_param_in()

    def _resolve_simulation_mode(self):
        simulation_mode_static_dir_path = \
                         self.root_path / self.static_dir / self.simulation_kind

        if simulation_mode_static_dir_path.is_dir():
            for static_file_path in simulation_mode_static_dir_path.glob('*'):
                self.link_file(static_file_path, relative=False)

    def _resolve_hotstart(self):

        hotstarted_msg = 'HOTSTARTED simulation run from %s!'
        coldstated_msg = 'COLDSTARTED simulation run! [reason: %s]'

        if self.force_coldstart:
            self.logger.warning(coldstated_msg, 'forced')
            return False

        if self.force_hotstart_path:
            self.logger.info(hotstarted_msg, self.force_hotstart_path)
            return True

        if not (self.series_interval and self.series_path_template):
            self.logger.info(coldstated_msg, 'no series')
            return False

        delta_steps = lambda other_dt: \
                                    (self.date_time - other_dt) // self.timestep
        hotstart_filename = lambda dt: f'{delta_steps(dt)}_hotstart.in.xz'
        hotstart_path_from_ = lambda dt: (
            Path( dt.strftime(self.series_path_template) ) /
                        self.latest_run_link / 'outputs' / hotstart_filename(dt)
        )

        hotstart_timedelta_range = utils.timedelta_range(self.period,
                                                     delta=self.series_interval)

        for hotstart_timedelta in hotstart_timedelta_range:
            hotstart_datetime = self.date_time - hotstart_timedelta
            hotstart_path = hotstart_path_from_(hotstart_datetime)

            self.logger.info('checking hotstart from %s at %s',
                                       hotstart_datetime, repr(hotstart_path) )
            if hotstart_path.is_file():
                break

        else:
            self.logger.warning(coldstated_msg, 'no restart file(s) available')
            return False

        self.hotstart_path = hotstart_path
        self.hotstart_datetime = hotstart_datetime

        self.logger.info(hotstarted_msg,
                                       f'{hotstart_datetime}, {hotstart_path}')
        return True

    # TODO: make it more generic
    def _populate_workspace(self):
        dirpath = os.path.relpath(self.path, self.root_path)
        ws_static_filepaths = self._workspace_filepaths(dirpath)

        workspace_linked_filepaths = list(
            self.link_file(file, symlink_file, relative=False)
                for file, symlink_file in zip(self._static_filepaths,
                                                            ws_static_filepaths)
        )

        if self.hotstart_path:
            ws_hotstart_link = self.link_file(self.hotstart_path,
                                                               'hotstart.in.xz')
            xz_args = '--decompress --keep --force --verbose'.split()
            self.run_executable(self.xz_bin, *xz_args, 'hotstart.in.xz')
            workspace_linked_filepaths.append(ws_hotstart_link)

        return workspace_linked_filepaths

    def _workspace_filepaths(self, dirpath):

        # workspace static files
        static_filepaths = self._define_filepaths(dirpath, self.static_files)
        self.logger.debug('workspace static files: %s', static_filepaths)

        return static_filepaths


    class CMEMSReginalDomain:

        GLOBAL = 'global'
        IBI = 'ibi'


    def _resolve_ocean_boundary_elev_forcings(self):

        forcing_resolver_map = {
            ForcingSource.PRISM_2017:
                              self._resolve_ocean_boundary_forcings_using_prism,
            ForcingSource.FES_2014:
                            self._resolve_ocean_boundary_forcings_using_fes2014,
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                self.CMEMSReginalDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_elev_forcings_using_cmems,
                                                   self.CMEMSReginalDomain.IBI),
        }

        try:
            forcing = self.forcings[cn.ForcingKind.OCEAN_ELEV]
        except KeyError:
            self.logger.info('No ocean elevation forcing defined!')
        else:
            forcing_resolver_map[forcing]()

    def _resolve_ocean_boundary_salt_temp_forcings(self):

        forcing_resolver_map = {
            ForcingSource.CMEMS_GLOBAL: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                self.CMEMSReginalDomain.GLOBAL),
            ForcingSource.CMEMS_IBI: partial(
                self._resolve_ocean_boundary_salt_temp_forcings_using_cmems,
                                                   self.CMEMSReginalDomain.IBI),
        }

        try:
            forcing = self.forcings[cn.ForcingKind.OCEAN_SALT_TEMP]
        except KeyError:
            pass
        else:
            forcing_resolver_map[forcing]()

    prism_dir = 'prism'

    def _resolve_ocean_boundary_forcings_using_prism(self):

        current_date = self.date_time.date()
        previous_date = current_date - timedelta(days=1)

        prism_file = lambda date, filename: f'{date:%Y-%j}/run/{filename}'
        prism_nesting_files = (
            ( '1_elev.61', prism_file(previous_date, 'outputs/1_elev.61') ),
            ( '2_elev.61', prism_file(current_date, 'outputs/1_elev.61') ),
            ( '3_elev.61', prism_file(current_date, 'outputs/2_elev.61') ),
            ( 'bg.gr3', prism_file(current_date, 'hgrid.gr3') ),
        )

        for link_name, suffix_path in prism_nesting_files:

            prism_file_path = self.static_path / self.prism_dir / suffix_path
            self.link_file(prism_file_path, link_name)

        # the remaining funcionality was handled by genBCschism.sh script

        enumerated_boundaries = enumerate(self.boundaries, start=1)
        is_ocean_ = lambda boundary: boundary['kind'] == BoundaryKind.OCEAN

        boundaries_index, boundaries_node_count = zip( *(
            (index, boundary['node_count']) for index, boundary
                                 in enumerated_boundaries if is_ocean_(boundary)
        ) )

        boundary_count = len(boundaries_index)
        boundaries_indexes_listing = utils.listing(boundaries_index)

        gen_fg_in = utils.format_lines(
            '{boundary_count}'
                  ' !total # of open boundary segments to be included in fg.bp',
            '{boundaries_index} !list of open boundary segment IDs',

            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
        )
        self.create_text_file(self.path/'gen_fg.in', gen_fg_in)
        self.run_executable('gen_fg')

        self.link_file('hgrid.ll', 'fg.gr3')
        self.link_file('vgrid.in', 'vgrid.fg')

        period_days = utils.days_in_(self.period)
        interval_days = utils.days_in_(self.series_interval)
        period_extended_days = period_days + interval_days

        interpolate_variables_in_file_type_dict = dict(
            elev2D = 1,
            salt3D = 2,
            temp3D = 2,
            uv3D = 3,
        )
        interpolate_variables_in = utils.format_lines(
            '{file_type} {length_days} ! file_type: {file_type_dict}; days',
            '{boundary_count} {boundaries_index}'
                                          ' ! boundary count; boundary indexes',
            '{debug:0} ! debug: 0=off, 1=on',

            length_days = period_extended_days,
            file_type = interpolate_variables_in_file_type_dict['elev2D'],
            file_type_dict = interpolate_variables_in_file_type_dict,
            boundary_count = boundary_count,
            boundaries_index = boundaries_indexes_listing,
            debug = False,
        )
        self.create_text_file(self.path/'interpolate_variables.in',
                                                       interpolate_variables_in)
        self.run_executable('interpolate_variables6')

        self.move_file('elev2D.th', 'th.old')

        header_template = utils.format_lines(
            '{{length_days}} 1 ! length days, number of vertical levels',
            '{boundary_count} {boundaries_node_count}'
                              ' ! ocean boundaries, node count in each of them',

            boundary_count = boundary_count,
            boundaries_node_count = utils.listing(boundaries_node_count),
        )

        slash_th_in = utils.format_lines(
            header_template.format(length_days=period_extended_days),
            '{interval_days} !number of days to slash from the beginning',

            interval_days = interval_days
        )
        self.create_text_file(self.path/'slash_th.in', slash_th_in)

        self.run_executable('slash_th')

        timeint_in = utils.format_lines(
            header_template.format(length_days=period_days),
            '{timestep} ! timestep',
            # '{vertical_offset} ! PRISM vertical offset',

            timestep = int( self.timestep.total_seconds() ),
            # vertical_offset = context.get('vertical_offset') or 0.
        )
        self.create_text_file(self.path/'timeint.in', timeint_in)

        #self.rename_local_file(path, 'th.new', 'th.old')

        self.run_executable('timeint_th', 'th.new', 'elev2D.th')

        #self.rename_local_file(path, 'th.new', 'elev2D.th')

        self.ocean_boundaries_files = prism_nesting_files

    def _resolve_ocean_boundary_forcings_using_fes2014(self):

        bnd_kind_spec_fmt = lambda details: \
             '' if details['kind'] == 'ocean' else ':{}'.format(details['flow'])
        format_boundary = lambda details: '{}{}'.format(
                                   details['kind'], bnd_kind_spec_fmt(details) )

        boundaries_specs = tuple( map(format_boundary, self.boundaries) )

        path = self.path
        end_date_time = self.date_time + self.period

        time_arg_fmt = '%Y-%m-%dT%H:%M:%SZ'

        script_args = (
            '-conf', self.static_path / 'slev_fes2014.ini',
            '--tstart', self.date_time.strftime(time_arg_fmt),
            '--tend', end_date_time.strftime(time_arg_fmt),
            #'--deltaT', self.timestep.total_seconds(),
            '--type', 'gr3',
            '--bnd', 'hgrid.ll',
            '--bnds_specs', *boundaries_specs,
            '--levels', str( self.vertical_levels_count or 2 ),
            '--velocities',
            '--out', 'bctides.in-fes2014',
            '--json'
        )

        self.run_executable(self.fes2014_exec, *script_args)

    def _resolve_ocean_boundary_elev_forcings_using_cmems(self,
                                                               regional_domain):

        cmems = self._cmems_forcings(regional_domain, cn.ForcingKind.OCEAN_ELEV)
        cmems.create_elev2D_th(
            date=self.date_time.date(),
            output_dir=self.path,
        )

    def _resolve_ocean_boundary_salt_temp_forcings_using_cmems(self,
                                                               regional_domain):

        cmems = self._cmems_forcings(regional_domain,
                                                 cn.ForcingKind.OCEAN_SALT_TEMP)

        create_args = dict(
            date=self.date_time.date(),
            output_dir=self.path,
        )
        cmems.create_SAL_3D_th(**create_args)
        cmems.create_TEM_3D_th(**create_args)

    @utils.QueuedExecution('cmems')
    def _cmems_forcings(self, regional_domain, kind):

        domain_dataset_map = {
            self.CMEMSReginalDomain.GLOBAL: {
                cn.ForcingKind.OCEAN_ELEV: 'global_afp_0p083_hourly',
                cn.ForcingKind.OCEAN_SALT_TEMP: 'global_afp_0p083_3d_daily',
            },
            self.CMEMSReginalDomain.IBI: {
                cn.ForcingKind.OCEAN_ELEV: 'ibi_afp_0p028_hourly',
                cn.ForcingKind.OCEAN_SALT_TEMP: 'ibi_afp_0p028_3d_daily',
            },
        }

        dataset_kind_map = domain_dataset_map[regional_domain]

        optional_args = dict()
        if self.vertical_levels_count:
            optional_args['depth_levels'] = self.vertical_levels_count

        return copernicus_cmems_Motu(
            dataset_key = dataset_kind_map[kind],
            update = time(0),
            period = self.period,
            extent = self.bbox,

            boundaries = self.boundaries,
            timestep = self.timestep,

            **optional_args,

            utils_dir = self.bin_path,
            templates_dir = self.static_path,
            elev_offset_nc = self.static_path / 'GLO-MFC_001_024_mdt.nc',
            motuclient = self.motuclient_exec,
        )

    def _resolve_atmospheric_forcings(self):

        forcings_map = {
            ForcingSource.NOAA_GFS_0P25:
                              self._resolve_atmospheric_forcings_using_noaa_gfs,
            ForcingSource.METEOFR_ARGPEGE_EA:
                        self._resolve_atmospheric_forcings_using_meteofr_arpege,
        }

        try:
            forcing_name = self.forcings[cn.ForcingKind.ATMOSPHERIC]

        except KeyError:
            pass

        else:
            if forcing_name:
                forcings_map[forcing_name]()

        self.logger.info('No atmospheric forcings defined!')

    _inverse_barometer_forcings = {
        ForcingSource.FES_2014,
    }

    @utils.QueuedExecution('noaa_gfs')
    def _resolve_atmospheric_forcings_using_noaa_gfs(self):

        sflux_dirpath = self._resolve_atmospheric_forcings_common()

        gfs = noaa_ncep_NOMADS('gfs_0p25_1hr', update=time(0), extent=self.bbox,
                                                             period=self.period)

        gfs.download(self.date_time.date(), 'air', dirpath=sflux_dirpath)
        if self.simulation_kind == SimulationKind.BAROCLINIC:
            gfs.download(self.date_time.date(), 'rad', dirpath=sflux_dirpath)

    @utils.QueuedExecution('meteofr_arpege')
    def _resolve_atmospheric_forcings_using_meteofr_arpege(self):

        sflux_dirpath = self._resolve_atmospheric_forcings_common()

        arpege = meteofrance_nwp_DCPC('arpege_0p1_1hr', update=time(0),
                                           extent=self.bbox, period=self.period)

        arpege.download(self.date_time.date(), 'air', dirpath=sflux_dirpath)

    def _resolve_atmospheric_forcings_common(self):

        sflux_dirpath = self.path / 'sflux'
        self.create_folder(sflux_dirpath)

        reference = self.forcings[cn.ForcingKind.ATMOSPHERIC]

        ocean_elev_forcing = self.forcings.get(cn.ForcingKind.OCEAN_ELEV)

        is_inverse_barometer = \
                          ocean_elev_forcing in self._inverse_barometer_forcings

        atmospheric_forcing_details = dict(
            reference = reference,
            timestep = timedelta(hours=1),
            use_inverse_barometer = int(is_inverse_barometer)
        )

        # TODO: avoid replacing previous info
        self.forcings[cn.ForcingKind.ATMOSPHERIC] = atmospheric_forcing_details

        return sflux_dirpath

    # TODO: should be a class on its own
    def _format_bctides_in(self):
        bctides_json_path = self.path / 'bctides.json'
        bctides_json = json.loads( bctides_json_path.read_bytes() ) \
                                       if bctides_json_path.exists() else dict()

        #base_date = context['date_time'].strftime('%m/%d/%Y %H:%M:%S')
        base_date = self.date_time.strftime('%Y-%m-%d %H:%M:%S')

        tidal_potential_header = \
                       '{freq_count} 0 ! nfreq tidal pot., cut-off depth for tp'
        tidal_potential_freqs = bctides_json.get('Tidal Potential') or ()
        tidal_potential = utils.format_lines(
            tidal_potential_header,
            *tidal_potential_freqs,

            freq_count = len(tidal_potential_freqs)
        )

        nodal_factors_header = '{freq_count} ! nfreq at bounds'
        nodal_factors_freqs = bctides_json.get('Nodal Factors') or ()
        nodal_factors = utils.format_lines(
            nodal_factors_header,
            *nodal_factors_freqs,

            freq_count = len(nodal_factors_freqs)
        )

        # def bctides_json_bnd_(kind):
        #     try:
        #         return bctides_json['Boundaries'][kind]
        #     except KeyError:
        #         return len(boundaries) * ( tuple(), )

        bctides_json_bnd_ = lambda kind: (
            bctides_json['Boundaries'][kind] if bctides_json
                                        else len(self.boundaries) * ( tuple(), )
        )

        bnd_elevations = bctides_json_bnd_('Elevations')
        bnd_velocities = bctides_json_bnd_('Velocities')

        boundary_header = (
            '{{node_count}} {elev_flag} {vel_flag} {temp_flag} {salt_flag}'
                     ' !{{kind}}: node count, elev, vel, temp and salt bc flags'
        )

        ocean_bc_flags = lambda bnd_index: dict(
            elev_flag = 3 if bnd_elevations[bnd_index] else 4,
            vel_flag = 5 if bnd_velocities[bnd_index] else 0,
        )
        river_bc_flags = dict(
            elev_flag = 0,
            vel_flag = 2,
        )
        barotropic_bc_flags = dict(
            temp_flag = 0,
            salt_flag = 0,
        )
        barotropic_boundary_template = {
                BoundaryKind.OCEAN: lambda bnd_index: (
                    boundary_header.format(
                        **ocean_bc_flags(bnd_index),
                        **barotropic_bc_flags,
                    ),
                    *bnd_elevations[bnd_index],
                    *bnd_velocities[bnd_index],
                ),
                BoundaryKind.RIVER: lambda _: (
                    boundary_header.format(
                        **river_bc_flags,
                        **barotropic_bc_flags,
                    ),
                    f'{{{ForcingKind.FLOW}}} !flux',
                ),
            }

        simulation_kind__boundary_templates__map = {

            SimulationKind.BAROTROPIC: barotropic_boundary_template,

            SimulationKind.BAROTROPIC_WAVES: barotropic_boundary_template,

            SimulationKind.BAROCLINIC: {
                BoundaryKind.OCEAN: lambda bnd_index: (
                    boundary_header.format(
                        **ocean_bc_flags(bnd_index),
                        temp_flag = 4,
                        salt_flag = 4,
                    ),
                    *bnd_elevations[bnd_index],
                    *bnd_velocities[bnd_index],
                    '1. ! temperature (TEM_3D.th) nudging factor',
                    '1. ! salinity (SAL_3D.th) nudging factor',
                ),
                BoundaryKind.RIVER: lambda _: (
                    boundary_header.format(
                        **river_bc_flags,
                        temp_flag = 2,
                        salt_flag = 2,

                    ),
                    f'{{{ForcingKind.FLOW}}} ! flux',
                    f'{{{ForcingKind.TEMPERATURE}}} ! temperature',
                    '1. ! temperature nudging factor',
                    f'{{{ForcingKind.SALINITY}}} ! salinity',
                    '1. ! salinity nudging factor',
                ),
            },

        }
        boundary_templates = simulation_kind__boundary_templates__map[
                                                           self.simulation_kind]
        format_boundary = lambda index, details: '\n'.join(
              boundary_templates[ details['kind'] ](index) ).format_map(details)

        open_boundaries_header = '{bnd_count} ! open boundary segments'
        open_boundaries = utils.format_lines(
            open_boundaries_header,
            *( format_boundary(*enum_details)
                               for enum_details in enumerate(self.boundaries) ),

            bnd_count = len(self.boundaries)
        )

        bctides_in_template_path = self.static_path / 'bctides.in.template'
        bctides_in = utils.format_lines(
            bctides_in_template_path.read_text(),

            BASE_DATE = base_date,
            TIDAL_POTENTIAL = tidal_potential,
            NODAL_FACTORS = nodal_factors,
            OPEN_BOUNDARIES = open_boundaries,
        )
        bctides_in_path = self.path / 'bctides.in'
        bctides_in_path.write_text(bctides_in)

        self.logger.info('formatted bctides.in from template %r',
                                                       bctides_in_template_path)

    timedelta_output = timedelta(hours=1)
    param_in_template_file = 'param.in.template'

    # the creation of the param.in file should be delayed as much as possible,
    # allowing changes to occur throughout the steps, even after this one, until
    # it's really needed; the necessary information could be gathered and stored
    # in the context, and only used when the file is rendered
    # TODO: should be a class on its own
    def _format_param_in(self):
        template_file = self.path / self.param_in_template_file
        template = template_file.read_text()

        timesteps_in_ = lambda timedelta: timedelta // self.timestep

        is_hotstarted = bool(self.hotstart_path)
        interval_timesteps = timesteps_in_(self.series_interval or 0)

        settings = dict(
            COORDINATE_SYSTEM = 1 if self.is_cartesian else 2,
            REFERENCE_LATITUDE = sum(self.bbox['lat']) / 2,
            HOTSTART_FLAG = int(is_hotstarted),

            PERIOD_DAYS = utils.days_in_(self.period),
            IS_RAMPED_UP = int(not is_hotstarted),
            RAMP_UP_DAYS = 1,

            TIMESTEP_SECONDS = int( self.timestep.total_seconds() ),

            TIMESTEPS_OUTPUT_SAMPLING = timesteps_in_(self.timedelta_output),
            TIMESTEPS_OUTPUT_FILES = interval_timesteps,
            TIMESTEPS_HOTSTART_FILES = interval_timesteps,

            YEAR = self.date_time.year,
            MONTH = self.date_time.month,
            DAY = self.date_time.day,
            HOUR = self.date_time.hour,
        )
        default_settings = dict(
            USE_INVERSE_BAROMETER = 0,
            ATMOSPHERIC_FORCING_MODE = 0,
            ATMOSPHERIC_FORCING_TIMESTEP_SECONDS = 0,
        )


        atmos_forcings = self.forcings['atmospheric']
        if atmos_forcings:
            use_inverse_barometer = \
                            atmos_forcings.get('use_inverse_barometer') or False
            timestep_seconds = atmos_forcings['timestep'].total_seconds()

            settings.update(
                USE_INVERSE_BAROMETER = int(use_inverse_barometer),
                ATMOSPHERIC_FORCING_MODE = 2, # sflux usage
                ATMOSPHERIC_FORCING_TIMESTEP_SECONDS = int(timestep_seconds)
            )

        else:
            self.logger.warning('NO atmospheric forcing will be used!')

        settings_chain = ChainMap(self.param_in, settings, default_settings)

        formatted = template.format_map(settings_chain)
        formatted_path = self.path / 'param.in'
        formatted_path.write_text(formatted)

        self.logger.info('formatted param.in from %r', template_file)


from collections import ChainMap
from datetime import timedelta
from pathlib import Path

from ..common import *
from .base import SCHISMStage


class Prepare1(SCHISMStage):

    simulation_kind = SCHISMStage.Input()
    boundaries = SCHISMStage.Input()
    is_cartesian = SCHISMStage.Input(False)
    force_coldstart = SCHISMStage.Input(False)

    bbox = SCHISMStage.Input() # get it from hgrid.ll

    default_station_outputs = (
        StationInOutputKind.ELEVATION,
    )

    stations = SCHISMStage.Input(dict)
    station_outputs = SCHISMStage.Input(default_station_outputs)
    # station_sample_interval = SCHISMStage.Input( timedelta(seconds=300) )

    latest_run_link = SCHISMStage.Input(optional=True)
    simulation_series_path_template = SCHISMStage.Input(optional=True)

    param_in = SCHISMStage.Input(dict, output=True)

    xz_bin = SCHISMStage.Input(default='/usr/bin/xz')
    bin_path = SCHISMStage.Input(optional=True, output=True)
    static_path = SCHISMStage.Input(optional=True, output=True)
    hotstart_path = SCHISMStage.Input(optional=True, output=True)
    hotstart_datetime = SCHISMStage.Input(optional=True, output=True)

    timestep = SCHISMStage.Output()

    sim_kind_static_files_map = {
        SimulationKind.BAROTROPIC: dict(
            vgrid_in = 'vgrid.in',
            param_in_template = 'param.in.template',
        ),
        SimulationKind.BAROTROPIC_WAVES: dict(
            vgrid_in = 'vgrid.in',
            param_in_template = 'param.in.template',
        ),
        SimulationKind.BAROCLINIC: dict(
            param_in_template = 'param.in.template',
        ),
    }

    default_bin_link = '_bin'
    default_static_link = '_static'
    default_schism_executable = 'schism'

    def execute(self):
        self.timestep = timedelta( seconds=int(self.param_in['dt']) )

        if self.bin_path:
            self.bin_path = self.link_file(self.bin_path, self.default_bin_link,
                                                                 relative=False)

        if self.static_path:
            self.static_path = self.link_file(self.static_path,
                                       self.default_static_link, relative=False)

        self._resolve_hotstart()
        self._resolve_dependencies()
        self._resolve_param_in()
        self._resolve_stations()

    def _resolve_hotstart(self):

        hotstarted_msg = 'HOTSTARTED simulation run from %s!'
        coldstated_msg = 'COLDSTARTED simulation run! [reason: %s]'

        if self.force_coldstart:
            self.logger.warning(coldstated_msg, 'forced')
            return False

        if self.hotstart_path:
            self.logger.info(hotstarted_msg, self.hotstart_path)
            return True

        if not (self.series_interval and self.simulation_series_path_template
                                                      and self.latest_run_link):
            self.logger.info(coldstated_msg, 'no series')
            return False

        delta_steps = lambda other_dt: \
                                    (self.date_time - other_dt) // self.timestep
        hotstart_filename = lambda dt: f'{delta_steps(dt)}_hotstart.in.xz'
        hotstart_path_from_ = lambda dt: (
            Path( dt.strftime(self.simulation_series_path_template) ) /
                        self.latest_run_link / 'outputs' / hotstart_filename(dt)
        )

        hotstart_timedelta_range = utils.timedelta_range(self.period,
                                                     delta=self.series_interval)

        for hotstart_timedelta in hotstart_timedelta_range:
            hotstart_datetime = self.date_time - hotstart_timedelta
            hotstart_path = hotstart_path_from_(hotstart_datetime)

            self.logger.info('checking hotstart from %s at %s',
                                               hotstart_datetime, hotstart_path)
            if hotstart_path.is_file():
                break

        else:
            self.logger.warning(coldstated_msg, 'no restart file(s) available!')
            return False

        self.hotstart_path = hotstart_path
        self.hotstart_datetime = hotstart_datetime

        self.logger.info(hotstarted_msg,
                                        f'{hotstart_datetime}, {hotstart_path}')
        return True

    def _resolve_dependencies(self):
        schism_bin_path = ( self.bin_path / self.simulation_kind /
                                                self.default_schism_executable )
        self.link_file(schism_bin_path, 'schism')

        self.static_files = dict(
            (key, Path(self.simulation_kind, name) ) for key, name in
                    self.sim_kind_static_files_map[self.simulation_kind].items()
        )
        for key in self.static_files.keys():
            self.link_static_file(key)

        if self.hotstart_path:
            
            if self.hotstart_path.suffix == '.xz':
                hotstart_link = self.link_file(self.hotstart_path,
                                               'hotstart.in.xz', relative=False)
                xz_args = '--decompress --keep --force --verbose'.split()
                self.run_executable(self.xz_bin, *xz_args, hotstart_link)

            else:
                self.link_file(self.hotstart_path, 'hotstart.in',
                                                                 relative=False)

    timedelta_output = timedelta(hours=1)

    # the creation of the param.in file should be delayed as much as possible,
    # allowing changes to occur throughout the steps, even after this one, until
    # it's really needed; the necessary information could be gathered and stored
    # in the context, and only used when the file is rendered
    # TODO: should be a class on its own
    def _resolve_param_in(self):
        timesteps_in_ = lambda duration: duration // self.timestep

        is_hotstarted = bool(self.hotstart_path)
        interval_timesteps = timesteps_in_(self.series_interval or self.period)

        settings = dict(
            # coordinate system
            ics = 1 if self.is_cartesian else 2,
            # reference latitude
            cpp_lat = sum(self.bbox['lat']) / 2,
            # hotstart flag
            ihot = int(is_hotstarted),

            # run days
            rnday = utils.days_in_(self.period),
            # ramped up
            nramp = int(not is_hotstarted),
            # ramp up day
            dramp = 1,

            # timestep
            dt = int( self.timestep.total_seconds() ),

            # output sampling timesteps
            nspool = timesteps_in_(self.timedelta_output),
            # output files timesteps
            ihfskip = interval_timesteps,
            # hotstart files timesteps
            hotout_write = interval_timesteps,

            sim_year = self.date_time.year,
            sim_month = self.date_time.month,
            sim_day = self.date_time.day,
            sim_hour = self.date_time.hour,
            sim_minute = self.date_time.minute,
            sim_second = self.date_time.second,
        )
        settings.update(self.param_in)

        self.param_in = settings

    station_output_interval = timedelta(seconds=300)

    def _resolve_stations(self):
        output_enabled = False
        if self.stations and self.station_outputs:
            output_enabled = True
            self._format_station_in()

        station_output_pace = self.station_output_interval / self.timestep
        self.param_in.update(
            iout_sta = int(output_enabled),
            nspool_sta = int(station_output_pace)
        )

    def _format_station_in(self):

        station_in_template = (
            '{outputs}',
            '{count}',
            '{stations}'
        )
        station_template = '{index:3} {x:11.6} {y:11.6} {z:9.3} ! {name}'

        output_flags = ( '1' if kind in self.station_outputs else '0'
                                               for kind in StationInOutputKind )
        
        station_lines = (
            station_template.format(index=index, name=name,
                                          x=coords[0], y=coords[1], z=coords[2])
                for index, (name, coords) in
                                       enumerate(self.stations.items(), start=1)
        )

        self.create_text_file('station.in',
            '\n'.join(station_in_template).format(
                outputs = ' '.join(output_flags),
                count = len(self.stations),
                stations = '\n'.join(station_lines)
            )
        )
