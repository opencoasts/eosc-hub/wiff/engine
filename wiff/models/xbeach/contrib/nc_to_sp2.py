# -*- coding: utf-8 -*-

# June 2020,    A.Nahon - anahon@lnec.pt
#######################
# usage: nc_to_sp2.py wwm wwm_sta_0001.nc cg "2,3,4"
# reads:
#   - wwm's wave spectrum from station outputs in netcdf format
#   - ww3's wave spectrum outputs in netcdf format
# write:
#   - swan *.sp2 files
#   - loclist.txt and filelist_*.txt files for XBeach

import datetime
import numpy as np
import sys
import netCDF4
from pyproj import Proj, transform


def wwm_time(time_since_t0, t0=datetime.datetime(1858, 11, 17, 0, 0, 0)):
    """
    create a datetime64 array from a duration in second since an initial date
    :param time_since_t0: time array in seconds
    :param t0: initial date
    :return: datetime64 array
    """
    time = np.array([t0 + datetime.timedelta(seconds=sec)
                     for sec in time_since_t0],
                    dtype='datetime64[s]')
    return time


def ww3_time(time_since_t0, t0=datetime.datetime(1990, 1, 1, 0, 0, 0)):
    """
    create a datetime64 array from a duration in second since an initial date
    :param time_since_t0: time array in days
    :param t0: initial date
    :return: datetime64 array
    """
    time = np.array([t0 + datetime.timedelta(days=day)
                     for day in time_since_t0],
                    dtype='datetime64[s]')
    return time


def delta_t(time):
    ind = range(len(time)-1)
    dt = np.zeros(np.size(time))
    for i in ind:
        dt[i] = (time[i+1]-time[i]).astype(np.float)
    dt[-1] = dt[-2]
    return np.ceil(dt).astype(int)


def sort_ind(vec):
    sorted_indices = sorted(range(len(vec)), key=lambda k: vec[k])
    return sorted_indices


def read_wwm_sta(sta):
    """
    create/update fields of the input dictionary corresponding to
    spectral outputs from WWM-III model
    :param sta: dictionary containing the path ('source') to a
                station output netcdf file create by WWM-III
    :return:
    """
    nc = netCDF4.Dataset(sta['source'])
    sta['nsta'] = nc.dimensions['nbstation'].size
    sta['nrec'] = nc.dimensions['ocean_time'].size
    sta['ndir'] = nc.dimensions['ndir'].size
    sta['nfreq'] = nc.dimensions['nfreq'].size
    sta['directions'] = nc.variables['SPDIR'][:]
    sta['directions'] = np.rint(np.rad2deg(sta['directions']))
    # CDIR to NDIR
    sta['directions'] = 270 - sta['directions']
    lt0 = np.where(sta['directions'] < 0)
    sta['directions'][lt0] += 360
    sta['frequencies'] = nc.variables['SPSIG'][:]
    sta['frequencies'] = sta['frequencies'] / 2 / np.pi
    sta['hs'] = nc.variables['HS'][:, :]
    sta['tm02'] = nc.variables['TM02'][:, :]
    sta['tp'] = nc.variables['TPP'][:, :]
    sta['dm'] = nc.variables['DM'][:, :]
    sta['dp'] = nc.variables['DPEAK'][:, :]
    sta['time'] = wwm_time(nc.variables['ocean_time'][:])
    sta['dt'] = delta_t(sta['time'])
    # AC(ocean_time, ndir, nfreq, nbstation)
    sta['variance_density'] = nc.variables['ACOUT_2D'][:, :, :, :]
    # m2/rad.s-1/rad -> m2/Hz/deg;
    sta['variance_density'] = \
        sta['variance_density'] * (np.pi / 180.) * 2 * np.pi
    sta['x'] = nc.variables['x']
    sta['y'] = nc.variables['y']
    return


def read_ww3_nc(spec):
    nc = netCDF4.Dataset(spec['source'])
    spec['nsta'] = nc.dimensions['station'].size
    spec['nrec'] = nc.dimensions['time'].size
    spec['ndir'] = nc.dimensions['direction'].size
    spec['nfreq'] = nc.dimensions['frequency'].size
    spec['directions'] = nc.variables['direction'][:]
    # add 180 for NDIR
    spec['directions'] += 180
    gt360 = np.where(spec['directions'] >= 360)
    spec['directions'][gt360] -= 360
    spec['frequencies'] = nc.variables['frequency'][:]
    spec['hs'] = np.ones([spec['nrec'], spec['nsta']])
    spec['tm02'] = spec['hs']
    spec['tp'] = spec['hs']
    spec['dm'] = spec['hs']
    spec['dp'] = spec['hs']
    spec['time'] = ww3_time(nc.variables['time'][:])
    spec['dt'] = delta_t(spec['time'])
    # efth(ime, station, ndir, nfreq)
    spec['variance_density'] = nc.variables['efth'][:, :, :, :]
    # m2/Hz/rad -> m2/Hz/deg;
    spec['variance_density'] = spec['variance_density'] * (np.pi / 180.)
    spec['lon'] = nc.variables['longitude'][:, :]
    spec['lat'] = nc.variables['latitude'][:, :]
    projections = {
        'ETRS89': Proj('epsg:3763'),
        'WGS84': Proj('epsg:4326')
    }
    spec['x'], spec['y'] = transform(
        projections['WGS84'], projections['ETRS89'],
        spec['lat'][0, :], spec['lon'][0, :]
    )
    return


def moment(energy, f, power):
    return np.trapz(y=energy*np.power(f, power), x=f)


def dir_freq_integral(energy, f, dtheta, powers=[0]):
    energy_dirsum = np.sum(energy, axis=1)
    results = []
    for power in powers:
        results = np.append(results,
                            moment(energy_dirsum, f, power) * dtheta)
    return results


def bulk_par(energy, frequencies, directions):
    ndir = len(directions)
    nfreq = len(frequencies)
    dtheta = directions[1] - directions[0]

    directions_fd = np.transpose(
        np.deg2rad(
            np.reshape(
                np.repeat(directions, nfreq),
                [ndir, nfreq]
            )))
    energy_cos = energy * np.cos(directions_fd)
    energy_sin = energy * np.sin(directions_fd)

    moments = dir_freq_integral(
        energy, frequencies, dtheta, powers=[-2, 0, 1, 2]
    )

    moment_cos = dir_freq_integral(
        energy_cos, frequencies, dtheta
    )

    moment_sin = dir_freq_integral(
        energy_sin, frequencies, dtheta
    )

    dp = directions[
        np.argmax(np.sum(energy, axis=0))
    ]

    hs = 4 * np.sqrt(moments[1])
    tpc = moments[0]*moments[2]/np.power(moments[1], 2)
    tm01 = moments[1] / moments[2]
    tm02 = np.sqrt(moments[1] / moments[3])
    dm = np.rad2deg(np.arctan2(moment_sin, moment_cos))
    if dm < 0:
        dm += 360
    return hs, tpc, tm01, tm02, dm, dp


def write_sp2(sta, suffix, stations, dir):
    print('var_dim: ', np.shape(sta['variance_density']))
    loclist = open(f'{dir}/loclist.txt', 'w')
    loclist.write('LOCLIST \n')
    print('stations: ', stations)
    for i in stations:
        station_list = 'filelist_' + str(i) + '.txt'
        bulk_param = open(f'{dir}/bulk_param_' + str(i) + '.txt', 'w')
        # bulk_param.write('# Date Time Hs Tpc Tm01 Tm02 Dm Dp \n')
        param_table = open(f'{dir}/param_table_' + str(i) + '.txt', 'w')
        loclist.write(
            ' ' + str(sta['x'][i - 1]).replace('[', '').replace(']', '') +
            ' ' + str(sta['y'][i - 1]).replace('[', '').replace(']', '') +
            ' ' + station_list + ' \n'
        )
        filelist = open(f'{dir}/{station_list}', 'w')
        filelist.write('FILELIST \n')
        station = suffix + '_' + str(i) + '_'
        j = -1
        for t in sta['time']:
            j += 1
            fsp2 = station + \
                   str(t).replace('-', '').replace(':', '').replace('T', '_')
            fsp2 = fsp2 + '.sp2'
            filelist.write(str(sta['dt'][j]) + ' 1 ' + fsp2 + ' \n')
            sp2 = open(f'{dir}/{fsp2}', 'w')
            sp2.write('SWAN 1 Swan standard spectral file \n')
            sp2.write('$ Data transformed from WWM-III station outputs \n')
            sp2.write('$ Hs: ' + str(sta['hs'][j, i - 1]) +
                      ' ; Tm02: ' + str(sta['tm02'][j, i - 1]) +
                      ' ; Tp: ' + str(sta['tp'][j, i - 1]) +
                      ' ; Dm: ' + str(sta['dm'][j, i - 1]) + ' \n')
            sp2.write('TIME \n')
            sp2.write(' 1 \n')
            sp2.write('LOCATIONS locations in x-y-space \n')
            sp2.write(' 1 \n')
            sp2.write(
                str(sta['x'][i - 1]).replace('[', '').replace(']', '') + ' ' +
                str(sta['y'][i - 1]).replace('[', '').replace(']', '') + '\n'
            )
            sp2.write('AFREQ \n')
            sp2.write(' ' + str(sta['nfreq']) + ' frequencies \n')
            for frequency in sta['frequencies']:
                sp2.write(' ' + str(frequency) + '\n')
            sp2.write('NDIR \n')
            sp2.write(' ' + str(sta['ndir']) + ' directions \n')
            ind_directions = sort_ind(sta['directions'])
            # uncomment for non-sorted directions
            # ind_directions = range(len(ind_directions))
            for ind_direction in ind_directions:
                sp2.write(' ' + str(sta['directions'][ind_direction]) + ' \n')

            sp2.write('QUANT \n')
            sp2.write(' 1 \n')
            sp2.write('VaDens \n')
            sp2.write('m2/Hz/degr \n')
            sp2.write(' -0.9900E+02 \n')
            sp2.write('FACTOR \n')  # as computed by X.Bertin
            if sta['model'] == 'wwm':
                spec = sta['variance_density'][j, :, :, i - 1]
                spec = np.transpose(spec)
                # if AC is used instead of ACOUT_2D, then uncomment next line
                # spec = spec * sta['frequencies'] * 2 * np.pi
            elif sta['model'] == 'ww3':
                spec = sta['variance_density'][j, i - 1, :, :]
            spec = spec[:, ind_directions]

            hs, tpc, tm01, tm02, dm, dp = bulk_par(
                spec, sta['frequencies'], sta['directions'][ind_directions]
            )
            bulk_param.write(
                str(t).replace('-', '').replace(':', '').replace('T', ' ') +
                ' ' + str(hs) +
                ' ' + str(tpc) +
                ' ' + str(tm01) +
                ' ' + str(tm02) +
                ' ' + str(dm).replace('[', '').replace(']', '') +
                ' ' + str(dp).replace('[', '').replace(']', '') + ' \n'
            )

            param_table.write(
                ' ' + str(hs) +
                ' ' + str(tpc) +
                ' ' + str(dm).replace('[', '').replace(']', '') +
                ' 3.3 10. ' + str(sta['dt'][j]) + ' 1\n'
            )

            factor = np.max(np.max(spec)) / 10000
            spec_norm = np.ceil(spec / factor - 1)
            spec_norm[np.where(spec_norm <= 0)] = 0
            spec_norm = spec_norm.astype('int')
            sp2.write(' ' + str(factor) + ' \n')
            # ind_direction = -1
            # for direction in sta['directions']:
            #   ind_direction += 1
            for ind_frequency in range(sta['nfreq']):
                # for ind_direction in ind_directions:
                for ind_direction in range(sta['ndir']):
                    sp2.writelines(' ' + str(spec_norm[ind_frequency,
                                                       ind_direction]))
                sp2.write('\n')
            del ind_directions
            sp2.close()
        param_table.close()
        filelist.close()
    loclist.close()
    return

def main(model, spectra_nc, case_id, stations, dir):
    spectra = {'source': spectra_nc,
               'model': model}
    if spectra['model'] == 'wwm':
        read_wwm_sta(spectra)
    elif spectra['model'] =='ww3':
        read_ww3_nc(spectra)
    else:
        exit('Please select a valid model key word: wwm or ww3')
    write_sp2(spectra, case_id, stations, dir)

if __name__ == "__main__":
    try:
        model, spectra_nc, case_id, stations_str = sys.argv[1:]
        stations_id = np.fromstring(stations_str, dtype=int, sep=',')
        main(model, spectra_nc, case_id, stations_id, '.')
    except ValueError as err:
        print('!!! ', err)
        exit('Please specify 4 arguments.')
